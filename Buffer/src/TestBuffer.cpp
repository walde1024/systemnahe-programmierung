#include "../includes/Buffer.h"
#include <iostream>

int main(int argc, char **argv) {

	Buffer*  buffer;

	if (argc < 2) {
		std::cout << "Please provide a file path as parameter";
	}
	else {
		buffer = new Buffer(*(argv+1));
		char c;
		while (true) {
			c = buffer->getChar();

			if (c != '\0') {
				std::cout << c;
			} else {
				break;
			}
		}
	}
}
