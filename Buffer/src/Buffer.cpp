/*
 * Buffer.cpp
 *
 *  Created on: Sep 26, 2012
 *      Author: knad0001
 */

#include "../includes/Buffer.h"


#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>
#include <unistd.h>
#include <iostream>

#define BLOCKSIZE 512
#define BUFFER_SIZE 8192


Buffer::Buffer(char *filePath) {

	currentCharPointer = 0;

	posix_memalign(&buffer, BLOCKSIZE, BUFFER_SIZE);
	textBuffer = (char*)buffer;
	// /home/walde/Downloads/SysProgTemplate_SS_15/read.txt
	handle = open(filePath, O_DIRECT);

	if (handle == -1) {
		fprintf (stderr, "\nerror opening file for buffer: %d\n", errno);
	}
}

bool Buffer::readNext() {
	bool hasRead = false;

	if (handle > -1) {
		//readBytes = read(handle, buffer, BUFFER_SIZE);
		//textBuffer = (char*)buffer;
		readBytes = read(handle, textBuffer, BUFFER_SIZE);
		if (readBytes == -1)
			hasRead = false;
		else
			hasRead = true;
	}
	else {
		readBytes = 0;
		hasRead = false;
	}

	currentCharPointer = 0;
	return hasRead;
}

char Buffer::getChar() {
	if (readBytes > 0 && readBytes > currentCharPointer) {
		char c =  *(textBuffer+currentCharPointer);
		currentCharPointer++;

		return c;
	}
	else {
		if (readNext()) {
			return getChar();
		}
	}

	return '\0';
}

char Buffer::ungetChar() {
	currentCharPointer = currentCharPointer-2;
	return getChar();
}

Buffer::~Buffer() {

}
