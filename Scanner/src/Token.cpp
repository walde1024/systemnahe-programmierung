/*
 * Token.cpp
 *
 *  Created on: Oct 18, 2015
 *      Author: walde
 */

#include "../includes/Token.h"
#include "../../Symboltable/includes/SymbolKey.h"
#include "../../Symboltable/includes/Symboltable.h"
#include "../includes/FileWriter.h"

#include <iostream>
#include <stdlib.h>
#include <stdio.h>


Token::Token(LangType type, SymbolKey* key, int line, int column, Symboltable* table) {
	mLangType = type;
	mSymbolKey = key;
	mLine = line;
	mColumn = column;

	mColStr = new char[1024];
	mLineStr = new char[1024];

	if (mLangType == SIGN) {
		findOutSignType(table);
	}
	else {
		mSignType = NO_SIGN;
	}
}

void Token::setValue(int value) {
	mValue = value;
}

int Token::getValue() {
	return mValue;
}

LangType Token::getLangType() {
	return mLangType;
}

SignType Token::getSignType() {
	return mSignType;
}

SymbolKey* Token::getSymbolKey() {
	return mSymbolKey;
}

void Token::setErrorSymbol(char symbol) {
	mErrorSymbol = symbol;
}

char Token::getErrorSymbol() {
	return mErrorSymbol;
}

int Token::getLine() {
	return mLine;
}

int Token::getColumn() {
	return mColumn;
}

void Token::printToConsole(Symboltable* table) {
	switch (mLangType) {
		case IDENTIFIER:
			std::cout << "IDENTIFIER \t";
			break;
		case INTEGER:
			std::cout << "INTEGER\t\t";
				break;
		case IF:
			std::cout << "IF\t\t\t";
				break;
		case WHILE:
			std::cout << "WHILE\t";
				break;
		case SIGN:
			std::cout << "SIGN\t\t";
				break;
		case COMMENT:
			std::cout << "COMMENT\t\t";
				break;
		default:
			break;
	}

	std::cout << "Line: " << mLine << " Column: " << mColumn;

	if (mLangType == IDENTIFIER) {
		std::cout << " \tLexem: " << table->getLexem(mSymbolKey);
	}
	else if (mLangType == INTEGER) {
		std::cout << " \tValue: " << mValue;
	}
}

void Token::printToFile(Symboltable* table, FileWriter* writer) {

	switch (mLangType) {
		case IDENTIFIER:
			writer->writeToFile("Token IDENTIFIER \t\t", 19);
			break;
		case INTEGER:
			writer->writeToFile("Token INTEGER\t\t\t", 16);
			break;
		case IF:
			writer->writeToFile("Token IF\t\t\t", 11);
			break;
		case WHILE:
			writer->writeToFile("Token WHILE\t\t", 13);
			break;
		case SIGN:
			writeSignTypeToFile(writer);
			break;
		case COMMENT:
			writer->writeToFile("Token COMMENT\t\t\t", 16);
			break;
		case WRITE:
			writer->writeToFile("Token WRITE\t\t\t", 14);
			break;
		case READ:
			writer->writeToFile("Token READ\t\t\t", 13);
			break;
		case ELSE:
			writer->writeToFile("Token ELSE\t\t\t", 13);
			break;
		case INT_KEYWORD:
			writer->writeToFile("Token INT_KEYWORD\t\t", 19);
			break;
		default:
			//Return if symbol error our out of bounds error
			return;
			break;
	}

	sprintf(mLineStr,"%d",mLine);
	sprintf(mColStr,"%d",mColumn);

	writer->writeToFile("Line: ", 6);
	writer->writeToFile(mLineStr);

	writer->writeToFile(" Column: ", 9);
	writer->writeToFile(mColStr);

	if (mLangType == IDENTIFIER) {
		writer->writeToFile(" \tLexem: ", 9);
		writer->writeToFile(table->getLexem(mSymbolKey));
	}
	else if (mLangType == INTEGER) {
		sprintf(mLineStr,"%d",mValue);
		writer->writeToFile(" \tValue: ", 9);
		writer->writeToFile(mLineStr);
	}

	writer->writeToFile("\n", 1);
}

void Token::writeSignTypeToFile(FileWriter* writer) {
	writer->writeToFile("Token ", 6);

	switch (mSignType) {
		case PLUS :
			writer->writeToFile("PLUS\t\t", 6);
			break;
		case MINUS :
			writer->writeToFile("MINUS\t\t\t", 8);
			break;
		case COLON :
			writer->writeToFile("COLON\t\t", 7);
			break;
		case STAR :
			writer->writeToFile("STAR\t\t\t", 7);
			break;
		case LEFT_ARROW :
			writer->writeToFile("LEFT_ARROW\t", 11);
			break;
		case RIGHT_ARROW:
			writer->writeToFile("RIGHT_ARROW\t", 12);
			break;
		case EQUALS:
			writer->writeToFile("EQUALS\t\t\t", 9);
			break;
		case COLON_EQUALS:
			writer->writeToFile("COLON_EQUALS\t", 13);
			break;
		case LEFT_ARROW_COLON_RIGHT_ARROW:
			writer->writeToFile("LEFT_ARROW_COLON_RIGHT_ARROW\t", 29);
			break;
		case EXCLAMATION:
			writer->writeToFile("EXCLAMATION\t", 12);
			break;
		case AND :
			writer->writeToFile("AND\t\t", 5);
			break;
		case SEMICOLON:
			writer->writeToFile("SEMICOLON\t\t\t", 12);
			break;
		case BRACKET_LEFT :
			writer->writeToFile("BRACKET_LEFT\t\t", 14);
			break;
		case BRACKET_RIGHT :
			writer->writeToFile("BRACKET_RIGHT\t\t", 15);
			break;
		case CURLY_BRACKET_LEFT :
			writer->writeToFile("CURLY_BRACKET_LEFT\t", 19);
			break;
		case CURLY_BRACKET_RIGHT :
			writer->writeToFile("CURLY_BRACKET_RIGHT\t", 20);
			break;
		case SQUARE_BRACKET_LEFT :
			writer->writeToFile("SQUARE_BRACKET_LEFT\t", 20);
			break;
		case SQUARE_BRACKET_RIGHT:
			writer->writeToFile("SQUARE_BRACKET_RIGHT\t", 21);
			break;
		case SLASH:
			writer->writeToFile("SLASH\t", 6);
			break;
		default:
			break;
	}
}

void Token::findOutSignType(Symboltable* table) {
	char* lexem = table->getLexem(mSymbolKey);
	if (*lexem == '+') {
		mSignType = PLUS;
	}
	else if (*lexem == '-') {
		mSignType = MINUS;
	}
	else if (*lexem == ':' && *(lexem+1) == '\0') {
		mSignType = COLON;
	}
	else if (*lexem == '*') {
		mSignType = STAR;
	}
	else if (*lexem == '<' && *(lexem+1) == '\0') {
		mSignType = LEFT_ARROW;
	}
	else if (*lexem == '>' && *(lexem+1) == '\0') {
		mSignType = RIGHT_ARROW;
	}
	else if (*lexem == '=' && *(lexem+1) == '\0') {
		mSignType = EQUALS;
	}
	else if (*lexem == ':' && *(lexem+1) == '=') {
		mSignType = COLON_EQUALS;
	}
	else if (*lexem == '<' && *(lexem+1) == ':' && *(lexem+2) == '>') {
		mSignType = LEFT_ARROW_COLON_RIGHT_ARROW;
	}
	else if (*lexem == '!') {
		mSignType = EXCLAMATION;
	}
	else if (*lexem == '&') {
		mSignType = AND;
	}
	else if (*lexem == ';') {
		mSignType = SEMICOLON;
	}
	else if (*lexem == '(') {
		mSignType = BRACKET_LEFT;
	}
	else if (*lexem == ')') {
		mSignType = BRACKET_RIGHT;
	}
	else if (*lexem == '{') {
		mSignType = CURLY_BRACKET_LEFT;
	}
	else if (*lexem == '}') {
		mSignType = CURLY_BRACKET_RIGHT;
	}
	else if (*lexem == '[') {
		mSignType = SQUARE_BRACKET_LEFT;
	}
	else if (*lexem == ']') {
		mSignType = SQUARE_BRACKET_RIGHT;
	}
	else if (*lexem == '/') {
		mSignType = SLASH;
	}
}

Token::~Token() {
	delete mColStr;
	delete mLineStr;
}

