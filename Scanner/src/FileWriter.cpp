/*
 * FileWriter.cpp
 *
 *  Created on: Oct 19, 2015
 *      Author: walde
 */

#include "../includes/FileWriter.h"
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <iostream>
#include <stdio.h>

FileWriter::FileWriter(char *filePath) {

	remove(filePath);
	handle = open(filePath, O_CREAT | O_WRONLY, 0666);
	if (handle == -1) {
		fprintf (stderr, "\nerror opening file for writer\n");
	}

	buffer = new char[bufferSize];
	mCurrentBufferPos = 0;
}

void FileWriter::writeToFile(char* b, int size) {

	if ((mCurrentBufferPos + size) < bufferSize) {
		for (int i = 0; i < size; i++) {
			buffer[mCurrentBufferPos+i] = *(b+i);
		}
		mCurrentBufferPos += size;
	}
	else {
		write(handle, buffer, mCurrentBufferPos);
		mCurrentBufferPos = 0;

		for (int i = 0; i < size; i++) {
			buffer[mCurrentBufferPos+i] = *(b+i);
		}
	}
}

void FileWriter::writeToFile(char* b) {
	int size = 0;
	while (*(b+size) != '\0') {
		size++;
	}

	writeToFile(b, size);
}

void FileWriter::flush() {
	write(handle, buffer, mCurrentBufferPos);
	mCurrentBufferPos = 0;
}

FileWriter::~FileWriter() {

}

