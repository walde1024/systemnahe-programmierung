#include "../includes/Scanner.h"
#include "../includes/FileWriter.h"
#include "../includes/Token.h"
#include <stdio.h>
#include <iostream>
#include <time.h>

#include "../../Symboltable/includes/Symboltable.h"

#include <cstdlib>

int main(int argc, char **argv) {

	clock_t time = clock();

	if (argc < 3) {
		fprintf (stderr, "\nPlease provide an input and outpufile. Example: './ScannerTest read.txt out.txt'\n");
		return EXIT_FAILURE;
	}

	Symboltable *tab = new Symboltable();
	FileWriter *writer = new FileWriter(argv[2]);
	Scanner *scanner = new Scanner(argv[1], tab, writer);

	Token *t;

	while (t = scanner->nextToken()) {

		if (t)
			t->printToFile(tab, writer);


		delete t;
		//i++;
	}

	scanner->flushFileWriter();

	time = clock() - time;
	std::cout << "\n\nExecution time: " << ((float)time)/CLOCKS_PER_SEC << " seconds\n\n";

	return EXIT_SUCCESS;
}

