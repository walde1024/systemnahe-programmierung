/*
 * Scanner.cpp
 *
 *  Created on: Sep 26, 2012
 *      Author: knad0001
 */

#include "../includes/Scanner.h"
#include "../../Automat/includes/AutomatWrapper.h"
#include "../../Buffer/includes/Buffer.h"
#include "../../Symboltable/includes/Symboltable.h"
#include "../includes/Token.h"
#include "../includes/FileWriter.h"
#include <stdlib.h>     /* strtol */
#include <errno.h>
#include <error.h>
#include <stdio.h>

#include <iostream>

Scanner::Scanner(char* inputFile, Symboltable* tab, FileWriter* writer) {
	mAutomat = new AutomatWrapper();
	mCurrentTokenBufferPosition = 0;
	mBuffer = new Buffer(inputFile);
	mCurrentBacktrackingBufferPosition = 0;
	mCurrentBacktrackingParametersCount = 0;
	mSymboltable = tab;
	mFileWriter = writer;
	mNextParameter = '\0';
}

Token* Scanner::nextToken() {
	char currentChar;

	while (mCurrentBacktrackingParametersCount > 0) {
		mCurrentToken = 0;

		currentChar = mCurrentBacktrackingBuffer[mCurrentBacktrackingBufferPosition];
		mCurrentBacktrackingBufferPosition++;
		mCurrentBacktrackingParametersCount--;

		if (currentChar == ' ')
			currentChar = '\n';

		mCurrentToken = 0;

		if (currentChar == '\n' || currentChar == '\0' || currentChar == '\t' ||  currentChar == '\r' ) {
			if (currentChar != '\r') {

				addedToBuffer = false;
				handleAutomat(' ');

				handleNewLine();
			}
		}
		else {
			addedToBuffer = true;
			pushToTokenBuffer(currentChar);
			handleAutomat(currentChar);
		}

		if (mCurrentToken) {
			return mCurrentToken;
		}
	}

	if (mNextParameter != '\0') {
		mCurrentToken = 0;
		char currentChar = mNextParameter;
		mNextParameter = '\0';

		handleAutomat(currentChar);

		if (mCurrentToken) {
			return mCurrentToken;
		}
	}

	while ((currentChar = mBuffer->getChar()) != '\0' || mCurrentTokenBufferPosition > 0) {
		mRead++;

		mCurrentToken = 0;

		if (currentChar == '\n' || currentChar == '\0' || currentChar == '\t' ||  currentChar == '\r' ) {
			if (currentChar == '\r')
				continue;

			addedToBuffer = false;
			handleAutomat(' ');

			handleNewLine();
		}
		else {
			addedToBuffer = true;
			pushToTokenBuffer(currentChar);
			handleAutomat(currentChar);
		}

		if (mCurrentToken) {
			return mCurrentToken;
		}
	}

	return 0;
}

Token* Scanner::handleAutomat(char parameter) {
	if (mAutomat->isTransitionPossible(parameter)) {
		mAutomat->performTransition(parameter);
		if (mAutomat->getTransitionCount() == 1) {
			mCol = mRead;
		}

		return 0;
	}
	else {
		if (mAutomat->isAccepting()) {
			LangType acceptedLangType = mAutomat->getAcceptedLangType();
			//**Accepting

			//The last symbol is the new one which wasn't accepted above.
			//We don't need it here.
			if (mCurrentTokenBufferPosition > 1 && addedToBuffer)
				mCurrentTokenBufferPosition--;
			//Don't create tokens for comments

			Token* tempToken = 0;
			if (acceptedLangType != COMMENT) {
				mCurrentToken = createToken(acceptedLangType);
			}

			mAutomat->resetToStartStates();

			mCurrentTokenBufferPosition= 0;
			pushToTokenBuffer(parameter);
			mNextParameter = parameter;
			//handleAutomat(parameter);
			return mCurrentToken;
		}
		else {
			if (mCurrentTokenBufferPosition > 1) {
				return handleBackTrackingMode();
			}
			else {
				Token *t = 0;
				if (parameter != ' ' && parameter != '\0') {
					fprintf (stderr, "\nunknown Token \t\tLine: %d Column: %d \tSymbol: %c", mLine, mRead, parameter);
					mCurrentToken = createToken(UNKNOWN_SYMBOL_ERROR);
				}
				mAutomat->resetToStartStates();
				mCurrentTokenBufferPosition = 0;

				return mCurrentToken;
			}
		}
	}
}

Token* Scanner::createToken(LangType type) {
	SymbolKey* key = 0;
	Token* t = 0;

	if (type == IDENTIFIER) {
		key = mSymboltable->addLexem(mCurrentTokenBuffer, mCurrentTokenBufferPosition);
		t = new Token(type, key, mLine, mCol, mSymboltable);
	}
	else if (type == INTEGER) {
		int value = getLexemIntegerValue();

		if (errno == ERANGE) {
			errno = 0;
			t = new Token(OUT_OF_RANGE_ERROR, key, mLine, mCol, mSymboltable);
		}
		else {
			t = new Token(type, key, mLine, mCol, mSymboltable);
			t->setValue(value);
		}
	}
	else if (type == UNKNOWN_SYMBOL_ERROR) {
		t = new Token(type, key, mLine, mCol, mSymboltable);
		t->setErrorSymbol(mCurrentTokenBuffer[mCurrentTokenBufferPosition]);
	}
	else if (type == SIGN) {
		key = mSymboltable->addLexem(mCurrentTokenBuffer, mCurrentTokenBufferPosition);
		t = new Token(type, key, mLine, mCol, mSymboltable);
	}
	else {
		t = new Token(type, key, mLine, mCol, mSymboltable);
	}

	return t;
}

//This is called when a transition was possible but not successfull.
//This implicates that the transition was called with the wrong parameter.
//If we can find a sign for e.g. which is accepted before this could work. (<)(:=)(>)
Token* Scanner::handleBackTrackingMode() {
	for (int i = mCurrentTokenBufferPosition - 1; i > 0; i--) {
		mAutomat->resetToStartStates();
		for (int j = 0; j < i; j++) {
			mAutomat->performTransition(mCurrentTokenBuffer[j]);
		}

		if (mAutomat->isAccepting()) {
			return handleBackTrackingModeAccepted(i);
		}
	}
}

Token* Scanner::handleBackTrackingModeAccepted(int unprocessedSymbolsIndex) {
	LangType acceptedLangType = mAutomat->getAcceptedLangType();
	int oldTokenBufferPosition = mCurrentTokenBufferPosition;

	//Set new buffer index to create the token the right way
	mCurrentTokenBufferPosition = unprocessedSymbolsIndex;

	//**Accepting
	//Don't create tokens for comments
	Token *t = 0;
	if (acceptedLangType != COMMENT) {
		t = createToken(acceptedLangType);
	}

	mAutomat->resetToStartStates();

	char currentBuffer[1024];
	int currentBufferPos = 0;
	mCurrentBacktrackingParametersCount = 0;
	mCurrentBacktrackingBufferPosition = 0;
	//unprocessedSymbolsIndex-1 Bytes leaving out because they were already processed
	for (int k = unprocessedSymbolsIndex; k < oldTokenBufferPosition; k++) {
		currentBuffer[currentBufferPos] = mCurrentTokenBuffer[k];
		currentBufferPos++;

		mCurrentBacktrackingBuffer[mCurrentBacktrackingParametersCount] = mCurrentTokenBuffer[k];
		mCurrentBacktrackingParametersCount++;
	}
	mCurrentTokenBufferPosition = 0;

	//Copy unprocessed symbols to currentTokenBuffer und perform transitions for them
	/*for (int i = 0; i < currentBufferPos; i++) {
		mCurrentTokenBuffer[i] = currentBuffer[i];

		if (i < currentBufferPos-1) {
			mAutomat->performTransition(currentBuffer[i]);
			if (mAutomat->getTransitionCount() == 1) {
				mCol = mCol + i + 1;
			}
		}
		else {
			handleLastBufferParameterAgain = true;
		}
	}
	mCurrentTokenBufferPosition = currentBufferPos;*/

	mCurrentToken = t;
	return mCurrentToken;
}

void Scanner::handleNewLine() {
	mCol = 0;
	mRead = 0;
	mLine++;
}

int Scanner::getLexemIntegerValue() {
	pushToTokenBuffer(' ');
	int result =  strtol (mCurrentTokenBuffer, 0, 10);
	if (errno == ERANGE) {
		fprintf (stderr, "\nInteger out of range \tLine: %d Column: %d", mLine, mCol);
	}

	return result;
}

void Scanner::pushToTokenBuffer(char parameter) {
	mCurrentTokenBuffer[mCurrentTokenBufferPosition] = parameter;
	mCurrentTokenBufferPosition++;

	if (mCurrentTokenBufferPosition >= mBufferSize) {
		increaseBuffer();
	}
}

void Scanner::increaseBuffer() {
	mBufferSize = mBufferSize * 2;
	char *newBuffer = new char[mBufferSize];

	for (int i = 0; i < mCurrentTokenBufferPosition; i++) {
		newBuffer[i] = mCurrentTokenBuffer[i];
	}

	mCurrentTokenBuffer = newBuffer;
}

void Scanner::flushFileWriter() {
	mFileWriter->flush();
}

Scanner::~Scanner() {
	delete mBuffer;
	delete mAutomat;
	delete mCurrentTokenBuffer;
	delete mFileWriter;
}
