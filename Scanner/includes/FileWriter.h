/*
 * FileWriter.h
 *
 *  Created on: Oct 19, 2015
 *      Author: walde
 */

#ifndef SCANNER_SRC_FILEWRITER_H_
#define SCANNER_SRC_FILEWRITER_H_

class FileWriter {
private:

	int handle;

	const int bufferSize = 16384;

	char* buffer;
	int mCurrentBufferPos;

public:
	FileWriter(char *filePath);
	virtual ~FileWriter();

	void writeToFile(char* b, int size);
	void writeToFile(char* b);

	void flush();
};

#endif /* SCANNER_SRC_FILEWRITER_H_ */
