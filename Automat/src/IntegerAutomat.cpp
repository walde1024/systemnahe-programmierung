/*
 * SignAutomat.cpp
 *
 *  Created on: Oct 14, 2015
 *      Author: walde
 */

#include "../includes/IntegerAutomat.h"
#include "../includes/states/IntegerStartState.h"

IntegerAutomat::IntegerAutomat() {
	setStartState(new IntegerStartState());
	setLangType(INTEGER);
}

IntegerAutomat::~IntegerAutomat() {

}

