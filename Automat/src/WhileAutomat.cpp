/*
 * SignAutomat.cpp
 *
 *  Created on: Oct 14, 2015
 *      Author: walde
 */

#include "../includes/WhileAutomat.h"
#include "../includes/states/WhileStartState.h"

WhileAutomat::WhileAutomat() {
	setStartState(new WhileStartState());
	setLangType(WHILE);
}

WhileAutomat::~WhileAutomat() {

}

