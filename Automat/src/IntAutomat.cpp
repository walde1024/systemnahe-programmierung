/*
 * IntAutomat.cpp
 *
 *  Created on: Nov 1, 2015
 *      Author: walde
 */

#include "../includes/IntAutomat.h"
#include "../includes/states/IntStartState.h"


IntAutomat::IntAutomat() {
	setStartState(new IntStartState());
	setLangType(INT_KEYWORD);
}

IntAutomat::~IntAutomat() {
	// TODO Auto-generated destructor stub
}

