/*
 * StatePool.cpp
 *
 *  Created on: Oct 28, 2015
 *      Author: walde
 */

#include "../includes/StatePool.h"
#include "../includes/states/CommentFinalState.h"
#include "../includes/states/CommentMiddleState.h"
#include "../includes/states/CommentStartState.h"
#include "../includes/states/IdentifierFinalState.h"
#include "../includes/states/IdentifierStartState.h"
#include "../includes/states/IfEndState.h"
#include "../includes/states/IfMiddleState.h"
#include "../includes/states/IfStartState.h"
#include "../includes/states/IntegerFinalState.h"
#include "../includes/states/IntegerStartState.h"
#include "../includes/states/NotAcceptedState.h"
#include "../includes/states/SignFinalState.h"
#include "../includes/states/SignStartState.h"
#include "../includes/states/SignThreeFirstState.h"
#include "../includes/states/SignThreeSecondState.h"
#include "../includes/states/SignTwoFirstState.h"
#include "../includes/states/WhileEndState.h"
#include "../includes/states/WhileMiddleState.h"
#include "../includes/states/WhileStartState.h"
#include "../includes/states/WriteEndState.h"
#include "../includes/states/WriteMiddleState.h"
#include "../includes/states/WriteStartState.h"
#include "../includes/states/ReadEndState.h"
#include "../includes/states/ReadMiddleState.h"
#include "../includes/states/ReadStartState.h"
#include "../includes/states/ElseEndState.h"
#include "../includes/states/ElseMiddleState.h"
#include "../includes/states/ElseStartState.h"
#include "../includes/states/IntEndState.h"
#include "../includes/states/IntMiddleState.h"
#include "../includes/states/IntStartState.h"

StatePool::StatePool() {

}

CommentFinalState* StatePool::mCommentFinalState = new CommentFinalState();
CommentMiddleState* StatePool::mCommentMiddleState = new CommentMiddleState('*');
CommentStartState* StatePool::mCommentStartState = new CommentStartState();
IdentifierFinalState* StatePool::mIdentifierFinalState = new IdentifierFinalState();
IdentifierStartState* StatePool::mIdentifierStartState = new IdentifierStartState();
IfEndState* StatePool::mIfEndState = new IfEndState();
IfMiddleState* StatePool::mIfMiddleState = new IfMiddleState('f');
IfStartState* StatePool::mIfStartState = new IfStartState();
IntegerFinalState* StatePool::mIntegerFinalState = new IntegerFinalState();
IntegerStartState* StatePool::mIntegerStartState = new IntegerStartState();
NotAcceptedState* StatePool::mNotAcceptedState = new NotAcceptedState();
SignFinalState* StatePool::mSignFinalState = new SignFinalState();
SignStartState* StatePool::mSignStartState = new SignStartState();
SignThreeFirstState* StatePool::mSignThreeFirstState = new SignThreeFirstState();
SignThreeSecondState* StatePool::mSignThreeSecondState = new SignThreeSecondState();
SignTwoFirstState* StatePool::mSignTwoFirstState = new SignTwoFirstState();
WhileEndState* StatePool::mWhileEndState = new WhileEndState();
WhileMiddleState* StatePool::mWhileMiddleState = new WhileMiddleState('h');
WhileStartState* StatePool::mWhileStartState = new WhileStartState();
WriteStartState* StatePool::mWriteStartState = new WriteStartState();
WriteMiddleState* StatePool::mWriteMiddleState = new WriteMiddleState();
WriteEndState* StatePool::mWriteEndState = new WriteEndState();
ReadStartState* StatePool::mReadStartState = new ReadStartState();
ReadMiddleState* StatePool::mReadMiddleState = new ReadMiddleState();
ReadEndState* StatePool::mReadEndState = new ReadEndState();
ElseStartState* StatePool::mElseStartState = new ElseStartState();
ElseMiddleState* StatePool::mElseMiddleState = new ElseMiddleState();
ElseEndState* StatePool::mElseEndState = new ElseEndState();
IntStartState* StatePool::mIntStartState = new IntStartState();
IntMiddleState* StatePool::mIntMiddleState = new IntMiddleState();
IntEndState* StatePool::mIntEndState = new IntEndState();

StatePool::~StatePool() {
	delete mCommentFinalState;
	delete mCommentMiddleState;
	delete mCommentStartState;
	delete mIdentifierFinalState;
	delete mIdentifierStartState;
	delete mIfEndState;
	delete mIfMiddleState;
	delete mIfStartState;
	delete mIntegerFinalState;
	delete mIntegerStartState;
	delete mNotAcceptedState;
	delete mSignFinalState;
	delete mSignStartState;
	delete mSignThreeFirstState;
	delete mSignThreeSecondState;
	delete mSignTwoFirstState;
	delete mWhileEndState;
	delete mWhileMiddleState;
	delete mWhileStartState;
	delete mWriteStartState;
	delete mWriteMiddleState;
	delete mWriteEndState;
	delete mReadStartState;
	delete mReadMiddleState;
	delete mReadEndState;
	delete mElseStartState;
	delete mElseMiddleState;
	delete mElseEndState;
	delete mIntStartState;
	delete mIntMiddleState;
	delete mIntEndState;
}

