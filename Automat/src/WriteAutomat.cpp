/*
 * WriteAutomat.cpp
 *
 *  Created on: Nov 1, 2015
 *      Author: walde
 */

#include "../includes/WriteAutomat.h"
#include "../includes/states/WriteStartState.h"


WriteAutomat::WriteAutomat() {
	setStartState(new WriteStartState());
	setLangType(WRITE);
}

WriteAutomat::~WriteAutomat() {
	// TODO Auto-generated destructor stub
}

