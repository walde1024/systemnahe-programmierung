/*
 * AutomatWrapper.cpp
 *
 *  Created on: Oct 14, 2015
 *      Author: walde
 */

#include "../includes/AutomatWrapper.h"
#include "../includes/SignAutomat.h"
#include "../includes/WhileAutomat.h"
#include "../includes/IdentifierAutomat.h"
#include "../includes/IntegerAutomat.h"
#include "../includes/IfAutomat.h"
#include "../includes/CommentAutomat.h"
#include "../includes/WriteAutomat.h"
#include "../includes/ReadAutomat.h"
#include "../includes/ElseAutomat.h"
#include "../includes/IntAutomat.h"

AutomatWrapper::AutomatWrapper() {
	// TODO Auto-generated constructor stub
	mMachines[0] = new SignAutomat();
	mMachines[1] = new WhileAutomat();
	mMachines[2] = new IfAutomat();
	mMachines[3] = new ElseAutomat();
	mMachines[4] = new IntAutomat();
	mMachines[5] = new IntegerAutomat();
	mMachines[6] = new WriteAutomat();
	mMachines[7] = new ReadAutomat();
	mMachines[8] = new IdentifierAutomat();
	mMachines[9] = new CommentAutomat();

	mTransitionCount = 0;
}

AutomatWrapper::~AutomatWrapper() {
	// TODO Auto-generated destructor stub
}

bool AutomatWrapper::isTransitionPossible(char parameter) {
	for (int i = 0; i < MACHINES_COUNT; i++) {
		if (mMachines[i]->isTransitionPossible(parameter)) {
			return true;
		}
	}

	return false;
}

bool AutomatWrapper::performTransition(char parameter) {
	bool performedTransition = false;

	for (int i = 0; i < MACHINES_COUNT; i++) {
		if (mMachines[i]->performTransition(parameter)) {
			performedTransition = true;
		}
	}

	if (performedTransition) {
		mTransitionCount++;
	}

	return performedTransition;
}

void AutomatWrapper::resetToStartStates() {
	mTransitionCount = 0;
	for (int i = 0; i < MACHINES_COUNT; i++) {
		mMachines[i]->resetToStartState();
	}
}

int AutomatWrapper::getTransitionCount() {
	return mTransitionCount;
}

bool AutomatWrapper::isAccepting() {
	for (int i = 0; i < MACHINES_COUNT; i++) {
		if (mMachines[i]->isInFinalState()) {
			return true;
		}
	}

	return false;
}

LangType AutomatWrapper::getAcceptedLangType() {
	for (int i = 0; i < MACHINES_COUNT; i++) {
		if (mMachines[i]->isInFinalState()) {
			return mMachines[i]->getLangType();
		}
	}

	return NOTHING;
}
