/*
 * SignAutomat.cpp
 *
 *  Created on: Oct 14, 2015
 *      Author: walde
 */

#include "../includes/CommentAutomat.h"
#include "../includes/states/CommentStartState.h"

CommentAutomat::CommentAutomat() {
	setStartState(new CommentStartState());
	setLangType(COMMENT);
}

CommentAutomat::~CommentAutomat() {

}

