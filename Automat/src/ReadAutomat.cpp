/*
 * ReadAutomat.cpp
 *
 *  Created on: Nov 1, 2015
 *      Author: walde
 */

#include "../includes/ReadAutomat.h"
#include "../includes/states/ReadStartState.h"

ReadAutomat::ReadAutomat() {
	setStartState(new ReadStartState());
	setLangType(READ);
}

ReadAutomat::~ReadAutomat() {
	// TODO Auto-generated destructor stub
}

