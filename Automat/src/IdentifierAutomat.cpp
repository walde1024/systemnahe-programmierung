/*
 * SignAutomat.cpp
 *
 *  Created on: Oct 14, 2015
 *      Author: walde
 */

#include "../includes/IdentifierAutomat.h"
#include "../includes/states/IdentifierStartState.h"

IdentifierAutomat::IdentifierAutomat() {
	setStartState(new IdentifierStartState());
	setLangType(IDENTIFIER);
}

IdentifierAutomat::~IdentifierAutomat() {

}

