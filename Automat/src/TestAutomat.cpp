#include "../includes/SignAutomat.h"
#include "../includes/IntegerAutomat.h"
#include "../includes/IdentifierAutomat.h"
#include "../includes/WhileAutomat.h"
#include "../includes/IfAutomat.h"
#include "../includes/AutomatWrapper.h"


#include "../includes/Automat.h"
#include "../includes/states/SignStartState.h"
#include <iostream>

int main (int argc, char* argv[]){
	AutomatWrapper *wrapper = new AutomatWrapper();
	wrapper->performTransition('i');
	wrapper->performTransition('f');
	wrapper->performTransition(' ');
	if (wrapper->isAccepting()) {
		std::cout << "Wrapper is accepting: if";
		std::cout << "\n" << "LangType: " << wrapper->getAcceptedLangType();
		std::cout << "\n" << "Is transition possible: " << wrapper->isTransitionPossible(' ');
	}

	wrapper->resetToStartStates();
	wrapper->performTransition('I');
	wrapper->performTransition('F');
	wrapper->performTransition(' ');
	if (wrapper->isAccepting()) {
		std::cout << "\n\nWrapper is accepting: IF";
		std::cout << "\n" << "LangType: " << wrapper->getAcceptedLangType();
				std::cout << "\n" << "Is transition possible: " << wrapper->isTransitionPossible(' ');
	}

	wrapper->resetToStartStates();
	wrapper->performTransition('w');
	wrapper->performTransition('h');
	wrapper->performTransition('i');
	wrapper->performTransition('l');
	wrapper->performTransition('e');
	wrapper->performTransition(' ');
	if (wrapper->isAccepting()) {
		std::cout << "\n\nWrapper is accepting: while";
		std::cout << "\n" << "LangType: " << wrapper->getAcceptedLangType();
		std::cout << "\n" << "Is transition possible: " << wrapper->isTransitionPossible(' ');
	}

	wrapper->resetToStartStates();
	wrapper->performTransition('W');
	wrapper->performTransition('H');
	wrapper->performTransition('I');
	wrapper->performTransition('L');
	wrapper->performTransition('E');
	wrapper->performTransition(' ');
	if (wrapper->isAccepting()) {
		std::cout << "\n\nWrapper is accepting: WHILE";
		std::cout << "\n" << "LangType: " << wrapper->getAcceptedLangType();
		std::cout << "\n" << "Is transition possible: " << wrapper->isTransitionPossible(' ');
	}

	wrapper->resetToStartStates();
	wrapper->performTransition('/');
	wrapper->performTransition('*');
	wrapper->performTransition('/');
	wrapper->performTransition('*');
	wrapper->performTransition('E');
	wrapper->performTransition('*');
	wrapper->performTransition('/');
	if (wrapper->isAccepting()) {
		std::cout << "\n\nWrapper is accepting: Comment";
		std::cout << "\n" << "LangType: " << wrapper->getAcceptedLangType();
		std::cout << "\n" << "Is transition possible: " << wrapper->isTransitionPossible(' ');
	}
}















































