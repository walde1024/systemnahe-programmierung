/*
 * SignAutomat.cpp
 *
 *  Created on: Oct 14, 2015
 *      Author: walde
 */

#include "../includes/IfAutomat.h"
#include "../includes/states/IfStartState.h"

IfAutomat::IfAutomat() {
	setStartState(new IfStartState());
	setLangType(IF);
}

IfAutomat::~IfAutomat() {

}

