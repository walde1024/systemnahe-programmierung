/*
 * CommentMiddleState.cpp
 *
 *  Created on: Oct 17, 2015
 *      Author: walde
 */

#include "../../includes/states/CommentMiddleState.h"
#include "../../includes/states/CommentFinalState.h"
#include "../../includes/states/NotAcceptedState.h"
#include "../../includes/Automat.h"
#include "../../includes/StatePool.h"

CommentMiddleState::CommentMiddleState(char nextTransitionParameter) {
	mNextTransitionParameter = nextTransitionParameter;
}

CommentMiddleState::~CommentMiddleState() {

}

bool CommentMiddleState::isTransitionPossibleWith(char parameter) {
	if (mNextTransitionParameter == '*' && parameter == '*') {
		return true;
	}
	else if (mNextTransitionParameter == '%') {
		return true;
	}
	else if (mNextTransitionParameter == '&') {
		return true;
	}

	return false;
}

bool CommentMiddleState::performTransition(char parameter, Automat* automat) {
	if (mNextTransitionParameter == '*' && parameter == '*') {
		StatePool::mCommentMiddleState->setNextTransitionParameter('%');
		automat->addNewState(StatePool::mCommentMiddleState);
		return true;
	}
	// % sign is placeholder for all ascii symbols
	else if (mNextTransitionParameter == '%' && parameter == '*') {
		StatePool::mCommentMiddleState->setNextTransitionParameter('&');
		automat->addNewState(StatePool::mCommentMiddleState);
		return true;
	}
	else if (mNextTransitionParameter == '%') {
		StatePool::mCommentMiddleState->setNextTransitionParameter('%');
		automat->addNewState(StatePool::mCommentMiddleState);
		return true;
	}
	else if (mNextTransitionParameter == '&' && parameter == ':') {
		automat->addNewState(StatePool::mCommentFinalState);
		return true;
	}
	else if (mNextTransitionParameter == '&') {
		StatePool::mCommentMiddleState->setNextTransitionParameter('%');
		automat->addNewState(StatePool::mCommentMiddleState);
		return true;
	}
	else {
		automat->addNewState(StatePool::mNotAcceptedState);
		return false;
	}

	return false;
}

void CommentMiddleState::setNextTransitionParameter(char parameter) {
	mNextTransitionParameter = parameter;
}

