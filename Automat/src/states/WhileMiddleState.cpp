/*
 * WhileMiddleState.cpp
 *
 *  Created on: Oct 15, 2015
 *      Author: walde
 */

#include "../../includes/states/WhileMiddleState.h"
#include "../../includes/states/WhileEndState.h"
#include "../../includes/states/NotAcceptedState.h"
#include "../../includes/Automat.h"
#include "../../includes/StatePool.h"

WhileMiddleState::WhileMiddleState(char nextTransitionParameter) {
	mNextTransitionParameter = nextTransitionParameter;
}

WhileMiddleState::~WhileMiddleState() {
	// TODO Auto-generated destructor stub
}

//True if current state can perform a transition with the delivered parameter.
bool WhileMiddleState::isTransitionPossibleWith(char parameter) {
	if (mNextTransitionParameter == parameter) {
		return true;
	}

	return false;
}

//True if transition could be executed.
bool WhileMiddleState::performTransition(char parameter, Automat* automat) {
	if (isTransitionPossibleWith(parameter)) {
		if (mNextTransitionParameter == 'h' && parameter == 'h') {
			StatePool::mWhileMiddleState->setNextTransitionParameter('i');
			automat->addNewState(StatePool::mWhileMiddleState);
		}
		else if (mNextTransitionParameter == 'i' && parameter == 'i') {
			StatePool::mWhileMiddleState->setNextTransitionParameter('l');
			automat->addNewState(StatePool::mWhileMiddleState);
		}
		else if (mNextTransitionParameter == 'l' && parameter == 'l') {
			StatePool::mWhileMiddleState->setNextTransitionParameter('e');
			automat->addNewState(StatePool::mWhileMiddleState);
		}
		else if (mNextTransitionParameter == 'e' && parameter == 'e') {
			automat->addNewState(StatePool::mWhileEndState);
		}
		else if (mNextTransitionParameter == 'H' && parameter == 'H') {
			StatePool::mWhileMiddleState->setNextTransitionParameter('I');
			automat->addNewState(StatePool::mWhileMiddleState);
		}
		else if (mNextTransitionParameter == 'I' && parameter == 'I') {
			StatePool::mWhileMiddleState->setNextTransitionParameter('L');
			automat->addNewState(StatePool::mWhileMiddleState);
		}
		else if (mNextTransitionParameter == 'L' && parameter == 'L') {
			StatePool::mWhileMiddleState->setNextTransitionParameter('E');
			automat->addNewState(StatePool::mWhileMiddleState);
		}
		else if (mNextTransitionParameter == 'E' && parameter == 'E') {
			automat->addNewState(StatePool::mWhileEndState);
		}

		return true;
	}
	else {
		automat->addNewState(StatePool::mNotAcceptedState);
		return false;
	}

	return false;
}

void WhileMiddleState::setNextTransitionParameter(char parameter) {
	mNextTransitionParameter = parameter;
}

