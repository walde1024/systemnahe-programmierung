/*
 * IntMiddleState.cpp
 *
 *  Created on: Nov 1, 2015
 *      Author: walde
 */

#include "../../includes/states/IntMiddleState.h"
#include "../../includes/states/IntEndState.h"
#include "../../includes/states/NotAcceptedState.h"

#include "../../includes/StatePool.h"
#include "../../includes/Automat.h"

IntMiddleState::IntMiddleState() {
	// TODO Auto-generated constructor stub

}

IntMiddleState::~IntMiddleState() {
	// TODO Auto-generated destructor stub
}

//True if current state can perform a transition with the delivered parameter.
bool IntMiddleState::isTransitionPossibleWith(char parameter) {
	if (mNextTransitionParameter == parameter) {
		return true;
	}

	return false;
}

//True if transition could be executed.
bool IntMiddleState::performTransition(char parameter, Automat* automat) {
	if (isTransitionPossibleWith(parameter)) {
		if (mNextTransitionParameter == 'n' && parameter == 'n') {
			StatePool::mIntMiddleState->setNextTransitionParameter('t');
			automat->addNewState(StatePool::mIntMiddleState);
		}
		else if (mNextTransitionParameter == 't' && parameter == 't') {
			automat->addNewState(StatePool::mIntEndState);
		}

		return true;
	}
	else {
		automat->addNewState(StatePool::mNotAcceptedState);
		return false;
	}

	return false;
}

void IntMiddleState::setNextTransitionParameter(char parameter) {
	mNextTransitionParameter = parameter;
}


