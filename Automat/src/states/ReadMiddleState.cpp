/*
 * ReadMiddleState.cpp
 *
 *  Created on: Nov 1, 2015
 *      Author: walde
 */

#include "../../includes/states/ReadMiddleState.h"
#include "../../includes/states/ReadEndState.h"
#include "../../includes/states/NotAcceptedState.h"

#include "../../includes/StatePool.h"
#include "../../includes/Automat.h"

ReadMiddleState::ReadMiddleState() {
	// TODO Auto-generated constructor stub

}

ReadMiddleState::~ReadMiddleState() {
	// TODO Auto-generated destructor stub
}

//True if current state can perform a transition with the delivered parameter.
bool ReadMiddleState::isTransitionPossibleWith(char parameter) {
	if (mNextTransitionParameter == parameter) {
		return true;
	}

	return false;
}

//True if transition could be executed.
bool ReadMiddleState::performTransition(char parameter, Automat* automat) {
	if (isTransitionPossibleWith(parameter)) {
		if (mNextTransitionParameter == 'e' && parameter == 'e') {
			StatePool::mReadMiddleState->setNextTransitionParameter('a');
			automat->addNewState(StatePool::mReadMiddleState);
		}
		else if (mNextTransitionParameter == 'a' && parameter == 'a') {
			StatePool::mReadMiddleState->setNextTransitionParameter('d');
			automat->addNewState(StatePool::mReadMiddleState);
		}
		else if (mNextTransitionParameter == 'd' && parameter == 'd') {
			automat->addNewState(StatePool::mReadEndState);
		}

		return true;
	}
	else {
		automat->addNewState(StatePool::mNotAcceptedState);
		return false;
	}

	return false;
}

void ReadMiddleState::setNextTransitionParameter(char parameter) {
	mNextTransitionParameter = parameter;
}

