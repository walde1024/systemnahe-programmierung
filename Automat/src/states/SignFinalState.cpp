/*
 * SignFinalState.cpp
 *
 *  Created on: Oct 14, 2015
 *      Author: walde
 */

#include "../../includes/states/SignFinalState.h"
#include "../../includes/Automat.h"
#include "../../includes/states/NotAcceptedState.h"
#include "../../includes/StatePool.h"


SignFinalState::SignFinalState() {
	// TODO Auto-generated constructor stub
	setIsFinalState(true);
}

SignFinalState::~SignFinalState() {
	// TODO Auto-generated destructor stub
}

//True if current state can perform a transition with the delivered parameter.
bool SignFinalState::isTransitionPossibleWith(char parameter) {
	return false;
}

//True if transition could be executed.
bool SignFinalState::performTransition(char parameter, Automat* automat) {
	automat->addNewState(StatePool::mNotAcceptedState);
	return false;
}

