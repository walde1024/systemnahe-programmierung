/*
 * CommentStartState.cpp
 *
 *  Created on: Oct 17, 2015
 *      Author: walde
 */

#include "../../includes/states/CommentStartState.h"
#include "../../includes/states/CommentMiddleState.h"
#include "../../includes/states/NotAcceptedState.h"
#include "../../includes/Automat.h"
#include "../../includes/StatePool.h"

CommentStartState::CommentStartState() {
	// TODO Auto-generated constructor stub

}

CommentStartState::~CommentStartState() {
	// TODO Auto-generated destructor stub
}

bool CommentStartState::isTransitionPossibleWith(char parameter) {
	if (parameter == ':') {
		return true;
	}

	return false;
}

bool CommentStartState::performTransition(char parameter, Automat* automat) {
	if (parameter == ':') {
		StatePool::mCommentMiddleState->setNextTransitionParameter('*');
		automat->addNewState(StatePool::mCommentMiddleState);
		return true;
	}
	else {
		automat->addNewState(StatePool::mNotAcceptedState);
		return false;
	}

	return false;
}

