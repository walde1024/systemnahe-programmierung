/*
 * WhileEndState.cpp
 *
 *  Created on: Oct 15, 2015
 *      Author: walde
 */

#include "../../includes/states/IfEndState.h"
#include "../../includes/states/NotAcceptedState.h"
#include "../../includes/Automat.h"
#include "../../includes/StatePool.h"

IfEndState::IfEndState() {
	setIsFinalState(true);
}

IfEndState::~IfEndState() {
	// TODO Auto-generated destructor stub
}

//True if current state can perform a transition with the delivered parameter.
bool IfEndState::isTransitionPossibleWith(char parameter) {
	return false;
}

//True if transition could be executed.
bool IfEndState::performTransition(char parameter, Automat* automat) {
	automat->addNewState(StatePool::mNotAcceptedState);
	return false;
}

