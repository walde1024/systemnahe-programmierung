/*
 * NotAcceptedState.cpp
 *
 *  Created on: Oct 16, 2015
 *      Author: walde
 */

#include "../../includes/states/NotAcceptedState.h"
#include "../../includes/Automat.h"


NotAcceptedState::NotAcceptedState() {
	// TODO Auto-generated constructor stub

}

NotAcceptedState::~NotAcceptedState() {
	// TODO Auto-generated destructor stub
}

//True if current state can perform a transition with the delivered parameter.
bool NotAcceptedState::isTransitionPossibleWith(char parameter) {
	return false;
}

//True if transition could be executed.
bool NotAcceptedState::performTransition(char parameter, Automat* automat) {
	return false;
}

