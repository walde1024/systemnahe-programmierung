/*
 * IntStartState.cpp
 *
 *  Created on: Nov 1, 2015
 *      Author: walde
 */

#include "../../includes/states/IntStartState.h"
#include "../../includes/states/IntMiddleState.h"
#include "../../includes/states/NotAcceptedState.h"
#include "../../includes/StatePool.h"
#include "../../includes/Automat.h"

IntStartState::IntStartState() {

}

IntStartState::~IntStartState() {

}

//True if current state can perform a transition with the delivered parameter.
bool IntStartState::isTransitionPossibleWith(char parameter) {
	if (parameter == 'i') {
		return true;
	}

	return false;
}

//True if transition could be executed.
bool IntStartState::performTransition(char parameter, Automat* automat) {

	if (parameter == 'i') {
		StatePool::mIntMiddleState->setNextTransitionParameter('n');
		automat->addNewState(StatePool::mIntMiddleState);

		return true;
	}
	else {
		automat->addNewState(StatePool::mNotAcceptedState);
		return false;
	}
}
