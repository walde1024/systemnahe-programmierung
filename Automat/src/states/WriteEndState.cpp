/*
 * WriteEndState.cpp
 *
 *  Created on: Nov 1, 2015
 *      Author: walde
 */

#include "../../includes/states/WriteEndState.h"
#include "../../includes/states/NotAcceptedState.h"

#include "../../includes/StatePool.h"
#include "../../includes/Automat.h"

WriteEndState::WriteEndState() {
	setIsFinalState(true);
}

WriteEndState::~WriteEndState() {
	// TODO Auto-generated destructor stub
}

//True if current state can perform a transition with the delivered parameter.
bool WriteEndState::isTransitionPossibleWith(char parameter) {
	return false;
}

//True if transition could be executed.
bool WriteEndState::performTransition(char parameter, Automat* automat) {
	automat->addNewState(StatePool::mNotAcceptedState);
	return false;
}

