/*
 * SignThreeFirstState.cpp
 *
 *  Created on: Oct 14, 2015
 *      Author: walde
 */

#include "../../includes/states/SignTwoFirstState.h"
#include "../../includes/states/SignFinalState.h"
#include "../../includes/states/NotAcceptedState.h"
#include "../../includes/StatePool.h"

#include "../../includes/Automat.h"

SignTwoFirstState::SignTwoFirstState() {
	// TODO Auto-generated constructor stub

}

SignTwoFirstState::~SignTwoFirstState() {
	// TODO Auto-generated destructor stub
}

//True if current state can perform a transition with the delivered parameter.
bool SignTwoFirstState::isTransitionPossibleWith(char parameter) {
	if (parameter == '=') {
		return true;
	}

	return false;
}

//True if transition could be executed.
bool SignTwoFirstState::performTransition(char parameter, Automat* automat) {
	if (parameter == '=') {
		automat->addNewState(StatePool::mSignFinalState);
		return true;
	}
	else {
		automat->addNewState(StatePool::mNotAcceptedState);
		return false;
	}

	return false;
}

