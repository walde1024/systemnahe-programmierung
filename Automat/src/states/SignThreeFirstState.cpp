/*
 * SignThreeFirstState.cpp
 *
 *  Created on: Oct 14, 2015
 *      Author: walde
 */

#include "../../includes/states/SignThreeFirstState.h"
#include "../../includes/states/SignThreeSecondState.h"
#include "../../includes/Automat.h"
#include "../../includes/states/NotAcceptedState.h"
#include "../../includes/StatePool.h"

SignThreeFirstState::SignThreeFirstState() {
	// TODO Auto-generated constructor stub

}

SignThreeFirstState::~SignThreeFirstState() {
	// TODO Auto-generated destructor stub
}

//True if current state can perform a transition with the delivered parameter.
bool SignThreeFirstState::isTransitionPossibleWith(char parameter) {
	if (parameter == ':') {
		return true;
	}

	return false;
}

//True if transition could be executed.
bool SignThreeFirstState::performTransition(char parameter, Automat* automat) {
	if (parameter == ':') {
		automat->addNewState(StatePool::mSignThreeSecondState);
		return true;
	}
	else {
		automat->addNewState(StatePool::mNotAcceptedState);
		return false;
	}


	return false;
}

