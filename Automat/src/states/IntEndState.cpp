/*
 * IntEndState.cpp
 *
 *  Created on: Nov 1, 2015
 *      Author: walde
 */

#include "../../includes/states/IntEndState.h"
#include "../../includes/states/NotAcceptedState.h"

#include "../../includes/StatePool.h"
#include "../../includes/Automat.h"

IntEndState::IntEndState() {
	// TODO Auto-generated constructor stub
	setIsFinalState(true);
}

IntEndState::~IntEndState() {
	// TODO Auto-generated destructor stub
}

//True if current state can perform a transition with the delivered parameter.
bool IntEndState::isTransitionPossibleWith(char parameter) {
	return false;
}

//True if transition could be executed.
bool IntEndState::performTransition(char parameter, Automat* automat) {
	automat->addNewState(StatePool::mNotAcceptedState);
	return false;
}
