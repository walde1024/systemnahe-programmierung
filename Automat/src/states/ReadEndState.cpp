/*
 * ReadEndState.cpp
 *
 *  Created on: Nov 1, 2015
 *      Author: walde
 */

#include "../../includes/states/ReadEndState.h"
#include "../../includes/states/NotAcceptedState.h"

#include "../../includes/StatePool.h"
#include "../../includes/Automat.h"

ReadEndState::ReadEndState() {
	setIsFinalState(true);
}

ReadEndState::~ReadEndState() {
	// TODO Auto-generated destructor stub
}

//True if current state can perform a transition with the delivered parameter.
bool ReadEndState::isTransitionPossibleWith(char parameter) {
	return false;
}

//True if transition could be executed.
bool ReadEndState::performTransition(char parameter, Automat* automat) {
	automat->addNewState(StatePool::mNotAcceptedState);
	return false;
}
