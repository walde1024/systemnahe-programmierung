/*
 * WriteStartState.cpp
 *
 *  Created on: Nov 1, 2015
 *      Author: walde
 */

#include "../../includes/states/WriteStartState.h"
#include "../../includes/states/WriteMiddleState.h"
#include "../../includes/states/NotAcceptedState.h"
#include "../../includes/StatePool.h"
#include "../../includes/Automat.h"

WriteStartState::WriteStartState() {
	// TODO Auto-generated constructor stub

}

WriteStartState::~WriteStartState() {
	// TODO Auto-generated destructor stub
}

//True if current state can perform a transition with the delivered parameter.
bool WriteStartState::isTransitionPossibleWith(char parameter) {
	if (parameter == 'w') {
		return true;
	}

	return false;
}

//True if transition could be executed.
bool WriteStartState::performTransition(char parameter, Automat* automat) {

	if (parameter == 'w') {
		StatePool::mWriteMiddleState->setNextTransitionParameter('r');
		automat->addNewState(StatePool::mWriteMiddleState);

		return true;
	}
	else {
		automat->addNewState(StatePool::mNotAcceptedState);
		return false;
	}
}


