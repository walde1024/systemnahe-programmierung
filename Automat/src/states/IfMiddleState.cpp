/*
 * WhileMiddleState.cpp
 *
 *  Created on: Oct 15, 2015
 *      Author: walde
 */
#include "../../includes/states/NotAcceptedState.h"
#include "../../includes/states/IfMiddleState.h"
#include "../../includes/states/IfEndState.h"
#include "../../includes/Automat.h"
#include "../../includes/StatePool.h"

IfMiddleState::IfMiddleState(char nextTransitionParameter) {
	mNextTransitionParameter = nextTransitionParameter;
}

IfMiddleState::~IfMiddleState() {
	// TODO Auto-generated destructor stub
}

//True if current state can perform a transition with the delivered parameter.
bool IfMiddleState::isTransitionPossibleWith(char parameter) {
	if (parameter == mNextTransitionParameter) {
		return true;
	}

	return false;
}

//True if transition could be executed.
bool IfMiddleState::performTransition(char parameter, Automat* automat) {
	if (mNextTransitionParameter == 'f' && parameter == 'f') {
		automat->addNewState(StatePool::mIfEndState);
		return true;
	}
	else if (mNextTransitionParameter == 'F' && parameter == 'F') {
		automat->addNewState(StatePool::mIfEndState);
		return true;
	}
	else {
		automat->addNewState(StatePool::mNotAcceptedState);
		return false;
	}

	return false;
}

void IfMiddleState::setNextTransitionParameter(char parameter) {
	mNextTransitionParameter = parameter;
}












