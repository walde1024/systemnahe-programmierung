/*
 * WriteMiddleState.cpp
 *
 *  Created on: Nov 1, 2015
 *      Author: walde
 */

#include "../../includes/states/WriteMiddleState.h"
#include "../../includes/states/WriteEndState.h"
#include "../../includes/states/NotAcceptedState.h"

#include "../../includes/StatePool.h"
#include "../../includes/Automat.h"

WriteMiddleState::WriteMiddleState() {
	// TODO Auto-generated constructor stub

}

WriteMiddleState::~WriteMiddleState() {
	// TODO Auto-generated destructor stub
}

//True if current state can perform a transition with the delivered parameter.
bool WriteMiddleState::isTransitionPossibleWith(char parameter) {
	if (mNextTransitionParameter == parameter) {
		return true;
	}

	return false;
}

//True if transition could be executed.
bool WriteMiddleState::performTransition(char parameter, Automat* automat) {
	if (isTransitionPossibleWith(parameter)) {
		if (mNextTransitionParameter == 'r' && parameter == 'r') {
			StatePool::mWriteMiddleState->setNextTransitionParameter('i');
			automat->addNewState(StatePool::mWriteMiddleState);
		}
		else if (mNextTransitionParameter == 'i' && parameter == 'i') {
			StatePool::mWriteMiddleState->setNextTransitionParameter('t');
			automat->addNewState(StatePool::mWriteMiddleState);
		}
		else if (mNextTransitionParameter == 't' && parameter == 't') {
			StatePool::mWriteMiddleState->setNextTransitionParameter('e');
			automat->addNewState(StatePool::mWriteMiddleState);
		}
		else if (mNextTransitionParameter == 'e' && parameter == 'e') {
			automat->addNewState(StatePool::mWriteEndState);
		}

		return true;
	}
	else {
		automat->addNewState(StatePool::mNotAcceptedState);
		return false;
	}

	return false;
}

void WriteMiddleState::setNextTransitionParameter(char parameter) {
	mNextTransitionParameter = parameter;
}



