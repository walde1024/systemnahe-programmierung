/*
 * WhileStartState.cpp
 *
 *  Created on: Oct 15, 2015
 *      Author: walde
 */

#include "../../includes/states/WhileStartState.h"
#include "../../includes/states/WhileMiddleState.h"
#include "../../includes/states/NotAcceptedState.h"
#include "../../includes/StatePool.h"

#include "../../includes/Automat.h"

WhileStartState::WhileStartState() {

}

WhileStartState::~WhileStartState() {
	// TODO Auto-generated destructor stub
}

//True if current state can perform a transition with the delivered parameter.
bool WhileStartState::isTransitionPossibleWith(char parameter) {
	if (parameter == 'W' || parameter == 'w') {
		return true;
	}

	return false;
}

//True if transition could be executed.
bool WhileStartState::performTransition(char parameter, Automat* automat) {
	if (isTransitionPossibleWith(parameter)) {
		if (parameter == 'W') {
			StatePool::mWhileMiddleState->setNextTransitionParameter('H');
			automat->addNewState(StatePool::mWhileMiddleState);
		}
		else if (parameter == 'w') {
			StatePool::mWhileMiddleState->setNextTransitionParameter('h');
			automat->addNewState(StatePool::mWhileMiddleState);
		}

		return true;
	}
	else {
		automat->addNewState(StatePool::mNotAcceptedState);
		return false;
	}

	return false;
}

