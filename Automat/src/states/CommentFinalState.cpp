/*
 * CommentFinalState.cpp
 *
 *  Created on: Oct 17, 2015
 *      Author: walde
 */

#include "../../includes/states/CommentFinalState.h"
#include "../../includes/states/NotAcceptedState.h"
#include "../../includes/Automat.h"
#include "../../includes/StatePool.h"

CommentFinalState::CommentFinalState() {
	setIsFinalState(true);
}

CommentFinalState::~CommentFinalState() {
	// TODO Auto-generated destructor stub
}

bool CommentFinalState::isTransitionPossibleWith(char parameter) {

	return false;
}

bool CommentFinalState::performTransition(char parameter, Automat* automat) {
	automat->addNewState(StatePool::mNotAcceptedState);
	return false;
}


