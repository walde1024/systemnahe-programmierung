/*
 * ElseMiddleState.cpp
 *
 *  Created on: Nov 1, 2015
 *      Author: walde
 */

#include "../../includes/states/ElseMiddleState.h"
#include "../../includes/states/ElseEndState.h"
#include "../../includes/states/NotAcceptedState.h"
#include "../../includes/Automat.h"
#include "../../includes/StatePool.h"

ElseMiddleState::ElseMiddleState() {
	// TODO Auto-generated constructor stub

}

ElseMiddleState::~ElseMiddleState() {
	// TODO Auto-generated destructor stub
}

//True if current state can perform a transition with the delivered parameter.
bool ElseMiddleState::isTransitionPossibleWith(char parameter) {
	if (mNextTransitionParameter == parameter) {
		return true;
	}

	return false;
}

//True if transition could be executed.
bool ElseMiddleState::performTransition(char parameter, Automat* automat) {
	if (isTransitionPossibleWith(parameter)) {
		if (mNextTransitionParameter == 'l' && parameter == 'l') {
			StatePool::mElseMiddleState->setNextTransitionParameter('s');
			automat->addNewState(StatePool::mElseMiddleState);
		}
		else if (mNextTransitionParameter == 's' && parameter == 's') {
			StatePool::mElseMiddleState->setNextTransitionParameter('e');
			automat->addNewState(StatePool::mElseMiddleState);
		}
		else if (mNextTransitionParameter == 'e' && parameter == 'e') {
			automat->addNewState(StatePool::mElseEndState);
		}
		else if (mNextTransitionParameter == 'L' && parameter == 'L') {
			StatePool::mElseMiddleState->setNextTransitionParameter('S');
			automat->addNewState(StatePool::mElseMiddleState);
		}
		else if (mNextTransitionParameter == 'S' && parameter == 'S') {
			StatePool::mElseMiddleState->setNextTransitionParameter('E');
			automat->addNewState(StatePool::mElseMiddleState);
		}
		else if (mNextTransitionParameter == 'E' && parameter == 'E') {
			automat->addNewState(StatePool::mElseEndState);
		}

		return true;
	}
	else {
		automat->addNewState(StatePool::mNotAcceptedState);
		return false;
	}

	return false;
}

void ElseMiddleState::setNextTransitionParameter(char parameter) {
	mNextTransitionParameter = parameter;
}
