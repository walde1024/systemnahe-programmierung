/*
 * IdentifierStartState.cpp
 *
 *  Created on: Oct 15, 2015
 *      Author: walde
 */

#include "../../includes/states/IdentifierStartState.h"
#include "../../includes/states/IdentifierFinalState.h"
#include "../../includes/states/NotAcceptedState.h"
#include "../../includes/Automat.h"
#include "../../includes/StatePool.h"

IdentifierStartState::IdentifierStartState() {
	// TODO Auto-generated constructor stub

}

IdentifierStartState::~IdentifierStartState() {
	// TODO Auto-generated destructor stub
}

//True if current state can perform a transition with the delivered parameter.
bool IdentifierStartState::isTransitionPossibleWith(char parameter) {
	char temp;
	for (int i = 0;; i++) {
		temp = *(this->possibleTransitionSymbols+i);
		if (temp == '\0') {
			return false;
		}
		else if (temp == parameter) {
			return true;
		}
	}

	return false;
}

//True if transition could be executed.
bool IdentifierStartState::performTransition(char parameter, Automat* automat) {
	if (isTransitionPossibleWith(parameter)) {
		automat->addNewState(StatePool::mIdentifierFinalState);
		return true;
	}
	else {
		automat->addNewState(StatePool::mNotAcceptedState);
		return false;
	}

	return false;
}
