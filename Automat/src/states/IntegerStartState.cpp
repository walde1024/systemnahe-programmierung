/*
 * IntegerStartState.cpp
 *
 *  Created on: Oct 15, 2015
 *      Author: walde
 */

#include "../../includes/states/IntegerStartState.h"
#include "../../includes/states/IntegerFinalState.h"
#include "../../includes/Automat.h"
#include "../../includes/states/NotAcceptedState.h"
#include "../../includes/StatePool.h"

IntegerStartState::IntegerStartState() {
	// TODO Auto-generated constructor stub

}

IntegerStartState::~IntegerStartState() {
	// TODO Auto-generated destructor stub
}

//True if current state can perform a transition with the delivered parameter.
bool IntegerStartState::isTransitionPossibleWith(char parameter) {
	char temp;
	for (int i = 0;; i++) {
		temp = *(this->possibleTransitionSymbols+i);
		if (temp == '\0') {
			return false;
		}
		else if (temp == parameter) {
			return true;
		}
	}

	return false;
}

	//True if transition could be executed.
bool IntegerStartState::performTransition(char parameter, Automat* automat) {
	if (isTransitionPossibleWith(parameter)) {
		automat->addNewState(StatePool::mIntegerFinalState);
		return true;
	}
	else {
		automat->addNewState(StatePool::mNotAcceptedState);
		return false;
	}

	return false;
}









