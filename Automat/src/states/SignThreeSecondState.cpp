/*
 * SignThreeFirstState.cpp
 *
 *  Created on: Oct 14, 2015
 *      Author: walde
 */

#include "../../includes/states/SignThreeSecondState.h"
#include "../../includes/states/SignFinalState.h"
#include "../../includes/Automat.h"
#include "../../includes/states/NotAcceptedState.h"
#include "../../includes/StatePool.h"

SignThreeSecondState::SignThreeSecondState() {
	// TODO Auto-generated constructor stub
	setIsFinalState(false);
}

bool SignThreeSecondState::isTransitionPossibleWith(char parameter) {
	if (parameter == '>') {
		return true;
	}

	return false;
}

	//True if transition could be executed.
bool SignThreeSecondState::performTransition(char parameter, Automat* automat) {
	if (parameter == '>') {
		automat->addNewState(StatePool::mSignFinalState);
		return true;
	}
	else {
		automat->addNewState(StatePool::mNotAcceptedState);
		return false;
	}

	return false;
}

SignThreeSecondState::~SignThreeSecondState() {
	// TODO Auto-generated destructor stub
}

