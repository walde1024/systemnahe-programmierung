/*
 * ElseEndState.cpp
 *
 *  Created on: Nov 1, 2015
 *      Author: walde
 */

#include "../../includes/states/ElseEndState.h"
#include "../../includes/states/NotAcceptedState.h"

#include "../../includes/StatePool.h"
#include "../../includes/Automat.h"

ElseEndState::ElseEndState() {
	setIsFinalState(true);
}

ElseEndState::~ElseEndState() {
	// TODO Auto-generated destructor stub
}

//True if current state can perform a transition with the delivered parameter.
bool ElseEndState::isTransitionPossibleWith(char parameter) {
	return false;
}

//True if transition could be executed.
bool ElseEndState::performTransition(char parameter, Automat* automat) {
	automat->addNewState(StatePool::mNotAcceptedState);
	return false;
}

