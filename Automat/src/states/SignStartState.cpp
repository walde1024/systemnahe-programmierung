#include "../../includes/states/SignStartState.h"
#include "../../includes/states/SignFinalState.h"
#include "../../includes/states/SignThreeFirstState.h"
#include "../../includes/states/SignTwoFirstState.h"
#include "../../includes/Automat.h"
#include "../../includes/states/NotAcceptedState.h"
#include "../../includes/StatePool.h"

SignStartState::SignStartState() {
	setIsFinalState(false);
}

//True if current state can perform a transition with the delivered parameter.
bool SignStartState::isTransitionPossibleWith(char parameter) {

	char temp;
	for (int i = 0;; i++) {
		temp = *(this->possibleTransitionSymbols+i);
		if (temp == '\0') {
			return false;
		}
		else if (temp == parameter) {
			return true;
		}
	}

	return false;
}

//True if transition could be executed.
bool SignStartState::performTransition(char parameter, Automat* automat) {
	if (isTransitionPossibleWith(parameter)) {

		//Can be <:> or just <
		if (parameter == '<') {
			automat->addNewState(StatePool::mSignFinalState);
			automat->addNewState(StatePool::mSignThreeFirstState);
		}
		//Can be : or :=
		else if (parameter == ':') {
			automat->addNewState(StatePool::mSignFinalState);
			automat->addNewState(StatePool::mSignTwoFirstState);
		}
		else {
			automat->addNewState(StatePool::mSignFinalState);
		}

		return true;
	}
	else {
		automat->addNewState(StatePool::mNotAcceptedState);
		return false;
	}

	return false;
}
