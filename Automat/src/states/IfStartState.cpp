/*
 * WhileStartState.cpp
 *
 *  Created on: Oct 15, 2015
 *      Author: walde
 */

#include "../../includes/states/IfStartState.h"
#include "../../includes/states/IfMiddleState.h"
#include "../../includes/states/NotAcceptedState.h"
#include "../../includes/Automat.h"
#include "../../includes/StatePool.h"

IfStartState::IfStartState() {

}

IfStartState::~IfStartState() {
	// TODO Auto-generated destructor stub
}

//True if current state can perform a transition with the delivered parameter.
bool IfStartState::isTransitionPossibleWith(char parameter) {
	if (parameter == 'i' || parameter == 'I') {
		return true;
	}

	return false;
}

//True if transition could be executed.
bool IfStartState::performTransition(char parameter, Automat* automat) {
	if (parameter == 'i') {
		StatePool::mIfMiddleState->setNextTransitionParameter('f');
		automat->addNewState(StatePool::mIfMiddleState);
		return true;
	}
	else if (parameter == 'I') {
		StatePool::mIfMiddleState->setNextTransitionParameter('F');
		automat->addNewState(StatePool::mIfMiddleState);
		return true;
	}
	else {
		automat->addNewState(StatePool::mNotAcceptedState);
		return false;
	}

	return false;
}

