/*
 * ReadStartState.cpp
 *
 *  Created on: Nov 1, 2015
 *      Author: walde
 */

#include "../../includes/states/ReadStartState.h"
#include "../../includes/states/ReadMiddleState.h"
#include "../../includes/states/NotAcceptedState.h"
#include "../../includes/StatePool.h"
#include "../../includes/Automat.h"

ReadStartState::ReadStartState() {
	// TODO Auto-generated constructor stub

}

ReadStartState::~ReadStartState() {
	// TODO Auto-generated destructor stub
}

//True if current state can perform a transition with the delivered parameter.
bool ReadStartState::isTransitionPossibleWith(char parameter) {
	if (parameter == 'r') {
		return true;
	}

	return false;
}

//True if transition could be executed.
bool ReadStartState::performTransition(char parameter, Automat* automat) {

	if (parameter == 'r') {
		StatePool::mReadMiddleState->setNextTransitionParameter('e');
		automat->addNewState(StatePool::mReadMiddleState);

		return true;
	}
	else {
		automat->addNewState(StatePool::mNotAcceptedState);
		return false;
	}
}
