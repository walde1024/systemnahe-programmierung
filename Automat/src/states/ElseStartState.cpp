/*
 * ElseStartState.cpp
 *
 *  Created on: Nov 1, 2015
 *      Author: walde
 */

#include "../../includes/states/ElseStartState.h"
#include "../../includes/states/ElseMiddleState.h"
#include "../../includes/states/NotAcceptedState.h"
#include "../../includes/StatePool.h"

#include "../../includes/Automat.h"

ElseStartState::ElseStartState() {
	// TODO Auto-generated constructor stub

}

ElseStartState::~ElseStartState() {
	// TODO Auto-generated destructor stub
}

//True if current state can perform a transition with the delivered parameter.
bool ElseStartState::isTransitionPossibleWith(char parameter) {
	if (parameter == 'E' || parameter == 'e') {
		return true;
	}

	return false;
}

//True if transition could be executed.
bool ElseStartState::performTransition(char parameter, Automat* automat) {
	if (isTransitionPossibleWith(parameter)) {
		if (parameter == 'E') {
			StatePool::mElseMiddleState->setNextTransitionParameter('L');
			automat->addNewState(StatePool::mElseMiddleState);
		}
		else if (parameter == 'e') {
			StatePool::mElseMiddleState->setNextTransitionParameter('l');
			automat->addNewState(StatePool::mElseMiddleState);
		}

		return true;
	}
	else {
		automat->addNewState(StatePool::mNotAcceptedState);
		return false;
	}

	return false;
}

