/*
 * ElseAutomat.cpp
 *
 *  Created on: Nov 1, 2015
 *      Author: walde
 */

#include "../includes/ElseAutomat.h"
#include "../includes/states/ElseStartState.h"

ElseAutomat::ElseAutomat() {
	setStartState(new ElseStartState());
	setLangType(ELSE);
}

ElseAutomat::~ElseAutomat() {
	// TODO Auto-generated destructor stub
}

