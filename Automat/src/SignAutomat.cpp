/*
 * SignAutomat.cpp
 *
 *  Created on: Oct 14, 2015
 *      Author: walde
 */

#include "../includes/SignAutomat.h"
#include "../includes/states/SignStartState.h"

SignAutomat::SignAutomat() {
	setStartState(new SignStartState());
	setLangType(SIGN);
}

SignAutomat::~SignAutomat() {

}

