/*
 * Automat.cpp
 *
 */

#include "../includes/Automat.h"
#include "../includes/states/State.h"

Automat::Automat() {

}

void Automat::clearCurrentStates() {
	/*for (int i = 0; i < mCurrentStatesCount; i++) {
		delete mCurrentStates[i];
	}*/

	mCurrentStatesCount = 0;
}

void Automat::addNewState(State* state) {
	mNewStates[mNewStatesCount] = state;
	mNewStatesCount++;
}

void Automat::addCurrentState(State* state) {
	mCurrentStates[mCurrentStatesCount] = state;
	mCurrentStatesCount++;
}

void Automat::setStartState(State* state) {
	mStartState = state;
	//addCurrentState(state);
}

bool Automat::isTransitionPossible(char parameter) {
	if (mCurrentStatesCount == 0) {
		if (mStartState->isTransitionPossibleWith(parameter)) {
			return true;
		}
	}
	else {
		for (int i = 0; i < mCurrentStatesCount; i++) {
			if (mCurrentStates[i]->isTransitionPossibleWith(parameter)) {
				return true;
			}
		}
	}

	return false;
}

bool Automat::performTransition(char parameter) {
	bool performedTransition = false;
	clearNewStates();

	if (mCurrentStatesCount == 0) {
		if (mStartState->performTransition(parameter, this)) {
			performedTransition = true;
		}
	}
	else {
		for (int i = 0; i < mCurrentStatesCount; i++) {
			if (mCurrentStates[i]->performTransition(parameter, this)) {
				performedTransition = true;
			}
		}
	}

	copyNewStatesToCurrentStates();

	return performedTransition;
}

void Automat::copyNewStatesToCurrentStates() {
	if (mNewStatesCount > 0) {
		clearCurrentStates();
		for (int i = 0; i < mNewStatesCount; i++) {
			addCurrentState(mNewStates[i]);
		}
	}
}

bool Automat::isInFinalState() {
	for (int i = 0; i < mCurrentStatesCount; i++) {
		if (mCurrentStates[i]->isFinalState()) {
			return true;
		}
	}

	return false;
}

void Automat::resetToStartState() {
	clearCurrentStates();
}

void Automat::clearNewStates() {
	mNewStatesCount = 0;
}

LangType Automat::getLangType() {
	return mLangType;
}

void Automat::setLangType(LangType type) {
	mLangType = type;
}

Automat::~Automat() {
	delete mStartState;
	for (int i = 0; i < MAX_STATES_COUNT; i++) {
		delete mCurrentStates[i];
	}
	delete mCurrentStates;
}
