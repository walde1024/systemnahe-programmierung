/*
 * IntAutomat.h
 *
 *  Created on: Nov 1, 2015
 *      Author: walde
 */

#ifndef AUTOMAT_SRC_INTAUTOMAT_H_
#define AUTOMAT_SRC_INTAUTOMAT_H_

#include "../includes/Automat.h"

class IntAutomat: public Automat {
public:
	IntAutomat();
	virtual ~IntAutomat();
};

#endif /* AUTOMAT_SRC_INTAUTOMAT_H_ */
