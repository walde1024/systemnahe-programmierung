/*
 * SignAutomat.h
 *
 *  Created on: Oct 14, 2015
 *      Author: walde
 */

#ifndef AUTOMAT_SRC_COMMENTAUTOMAT_H_
#define AUTOMAT_SRC_COMMENTAUTOMAT_H_

#include "Automat.h"

class CommentAutomat: public Automat {
public:
	CommentAutomat();
	virtual ~CommentAutomat();
};

#endif /* AUTOMAT_SRC_COMMENTAUTOMAT_H_ */
