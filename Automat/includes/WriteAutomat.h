/*
 * WriteAutomat.h
 *
 *  Created on: Nov 1, 2015
 *      Author: walde
 */

#ifndef AUTOMAT_SRC_WRITEAUTOMAT_H_
#define AUTOMAT_SRC_WRITEAUTOMAT_H_

#include "../includes/Automat.h"

class WriteAutomat: public Automat {
public:
	WriteAutomat();
	virtual ~WriteAutomat();
};

#endif /* AUTOMAT_SRC_WRITEAUTOMAT_H_ */
