/*
 * SignAutomat.h
 *
 *  Created on: Oct 14, 2015
 *      Author: walde
 */

#ifndef AUTOMAT_SRC_SIGNAUTOMAT_H_
#define AUTOMAT_SRC_SIGNAUTOMAT_H_

#include "Automat.h"

class SignAutomat: public Automat {
public:
	SignAutomat();
	virtual ~SignAutomat();
};

#endif /* AUTOMAT_SRC_SIGNAUTOMAT_H_ */
