/*
 * StatePool.h
 *
 *  Created on: Oct 28, 2015
 *      Author: walde
 */

#ifndef AUTOMAT_SRC_STATEPOOL_H_
#define AUTOMAT_SRC_STATEPOOL_H_

class CommentFinalState;
class CommentMiddleState;
class CommentStartState;
class IdentifierFinalState;
class IdentifierStartState;
class IfEndState;
class IfMiddleState;
class IfStartState;
class IntegerFinalState;
class IntegerStartState;
class NotAcceptedState;
class SignFinalState;
class SignStartState;
class SignThreeFirstState;
class SignThreeSecondState;
class SignTwoFirstState;
class WhileEndState;
class WhileMiddleState;
class WhileStartState;
class WriteStartState;
class WriteMiddleState;
class WriteEndState;
class ReadStartState;
class ReadMiddleState;
class ReadEndState;
class ElseStartState;
class ElseMiddleState;
class ElseEndState;
class IntStartState;
class IntMiddleState;
class IntEndState;

class StatePool {
public:
	StatePool();
	virtual ~StatePool();

	static CommentFinalState 		*mCommentFinalState;
	static CommentMiddleState 		*mCommentMiddleState;
	static CommentStartState 		*mCommentStartState;
	static IdentifierFinalState 	*mIdentifierFinalState;
	static IdentifierStartState 	*mIdentifierStartState;
	static IfEndState 				*mIfEndState;
	static IfMiddleState 			*mIfMiddleState;
	static IfStartState 			*mIfStartState;
	static IntegerFinalState 		*mIntegerFinalState;
	static IntegerStartState 		*mIntegerStartState;
	static NotAcceptedState 		*mNotAcceptedState;
	static SignFinalState 			*mSignFinalState;
	static SignStartState 			*mSignStartState;
	static SignThreeFirstState 		*mSignThreeFirstState;
	static SignThreeSecondState 	*mSignThreeSecondState;
	static SignTwoFirstState 		*mSignTwoFirstState;
	static WhileEndState 			*mWhileEndState;
	static WhileMiddleState 		*mWhileMiddleState;
	static WhileStartState 			*mWhileStartState;
	static WriteStartState			*mWriteStartState;
	static WriteMiddleState			*mWriteMiddleState;
	static WriteEndState			*mWriteEndState;
	static ReadStartState			*mReadStartState;
	static ReadMiddleState			*mReadMiddleState;
	static ReadEndState				*mReadEndState;
	static ElseStartState			*mElseStartState;
	static ElseMiddleState			*mElseMiddleState;
	static ElseEndState				*mElseEndState;
	static IntStartState			*mIntStartState;
	static IntMiddleState			*mIntMiddleState;
	static IntEndState				*mIntEndState;
};

#endif /* AUTOMAT_SRC_STATEPOOL_H_ */
