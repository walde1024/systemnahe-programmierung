/*
 * SignAutomat.h
 *
 *  Created on: Oct 14, 2015
 *      Author: walde
 */

#ifndef AUTOMAT_SRC_IFAUTOMAT_H_
#define AUTOMAT_SRC_IFAUTOMAT_H_

#include "Automat.h"

class IfAutomat: public Automat {
public:
	IfAutomat();
	virtual ~IfAutomat();
};

#endif /* AUTOMAT_SRC_IFAUTOMAT_H_ */
