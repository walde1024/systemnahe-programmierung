/*
 * ElseMiddleState.h
 *
 *  Created on: Nov 1, 2015
 *      Author: walde
 */

#ifndef AUTOMAT_SRC_STATES_ELSEMIDDLESTATE_H_
#define AUTOMAT_SRC_STATES_ELSEMIDDLESTATE_H_

#include "State.h"

class ElseMiddleState: public State {
private:

	char mNextTransitionParameter;

public:
	ElseMiddleState();
	virtual ~ElseMiddleState();

	//True if current state can perform a transition with the delivered parameter.
	virtual bool isTransitionPossibleWith(char parameter);

	//True if transition could be executed.
	virtual bool performTransition(char parameter, Automat* automat);

	void setNextTransitionParameter(char parameter);
};

#endif /* AUTOMAT_SRC_STATES_ELSEMIDDLESTATE_H_ */
