/*
 * SignStartState.h
 *
 *  Created on: Oct 14, 2015
 *      Author: walde
 */

#ifndef AUTOMAT_INCLUDES_SIGNSTARTSTATE_H_
#define AUTOMAT_INCLUDES_SIGNSTARTSTATE_H_

#include "State.h"

class SignStartState : public State {
private:

	char *possibleTransitionSymbols = "+-*:<>=:<!&;(){}[]";

public:
	SignStartState();

	//True if current state can perform a transition with the delivered parameter.
	virtual bool isTransitionPossibleWith(char parameter);

	//True if transition could be executed.
	virtual bool performTransition(char parameter, Automat* automat);
};


#endif /* AUTOMAT_INCLUDES_SIGNSTARTSTATE_H_ */
