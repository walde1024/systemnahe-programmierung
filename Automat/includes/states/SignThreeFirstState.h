/*
 * SignThreeFirstState.h
 *
 *  Created on: Oct 14, 2015
 *      Author: walde
 */

#ifndef AUTOMAT_SRC_STATES_SIGNTHREEFIRSTSTATE_H_
#define AUTOMAT_SRC_STATES_SIGNTHREEFIRSTSTATE_H_

#include "State.h"

class SignThreeFirstState: public State {
public:
	SignThreeFirstState();
	virtual ~SignThreeFirstState();

	//True if current state can perform a transition with the delivered parameter.
	virtual bool isTransitionPossibleWith(char parameter);

	//True if transition could be executed.
	virtual bool performTransition(char parameter, Automat* automat);
};

#endif /* AUTOMAT_SRC_STATES_SIGNTHREEFIRSTSTATE_H_ */
