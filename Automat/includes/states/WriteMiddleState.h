/*
 * WriteMiddleState.h
 *
 *  Created on: Nov 1, 2015
 *      Author: walde
 */

#ifndef AUTOMAT_INCLUDES_STATES_WRITEMIDDLESTATE_H_
#define AUTOMAT_INCLUDES_STATES_WRITEMIDDLESTATE_H_

#include "State.h"

class WriteMiddleState: public State {
private:

	char mNextTransitionParameter;

public:
	WriteMiddleState();
	virtual ~WriteMiddleState();

	//True if current state can perform a transition with the delivered parameter.
	virtual bool isTransitionPossibleWith(char parameter);

	//True if transition could be executed.
	virtual bool performTransition(char parameter, Automat* automat);

	void setNextTransitionParameter(char parameter);
};

#endif /* AUTOMAT_INCLUDES_STATES_WRITEMIDDLESTATE_H_ */
