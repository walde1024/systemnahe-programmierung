/*
 * WriteEndState.h
 *
 *  Created on: Nov 1, 2015
 *      Author: walde
 */

#ifndef AUTOMAT_INCLUDES_STATES_WRITEENDSTATE_H_
#define AUTOMAT_INCLUDES_STATES_WRITEENDSTATE_H_

#include "State.h"

class WriteEndState: public State {
public:
	WriteEndState();
	virtual ~WriteEndState();

	//True if current state can perform a transition with the delivered parameter.
	virtual bool isTransitionPossibleWith(char parameter);

	//True if transition could be executed.
	virtual bool performTransition(char parameter, Automat* automat);
};

#endif /* AUTOMAT_INCLUDES_STATES_WRITEENDSTATE_H_ */
