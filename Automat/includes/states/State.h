/*
 * State.h
 *
 *  Created on: Oct 14, 2015
 *      Author: walde
 */

#ifndef AUTOMAT_INCLUDES_STATE_H_
#define AUTOMAT_INCLUDES_STATE_H_

class Automat;

class State {
private:

	bool mIsFinalState = false;

public:
	//True if current state can perform a transition with the delivered parameter.
	virtual bool isTransitionPossibleWith(char parameter) = 0;

	//True if transition could be executed.
	virtual bool performTransition(char parameter, Automat* automat) = 0;

	void setIsFinalState(bool isFinal){mIsFinalState = isFinal;};

	bool isFinalState(){return mIsFinalState;}

	virtual ~State(){};
};


#endif /* AUTOMAT_INCLUDES_STATE_H_ */
