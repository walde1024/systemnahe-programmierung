/*
 * WhileStartState.h
 *
 *  Created on: Oct 15, 2015
 *      Author: walde
 */

#ifndef AUTOMAT_SRC_STATES_IFSTARTSTATE_H_
#define AUTOMAT_SRC_STATES_IFSTARTSTATE_H_

#include "State.h"

class IfStartState: public State {
public:
	IfStartState();
	virtual ~IfStartState();

	//True if current state can perform a transition with the delivered parameter.
	virtual bool isTransitionPossibleWith(char parameter);

	//True if transition could be executed.
	virtual bool performTransition(char parameter, Automat* automat);
};

#endif /* AUTOMAT_SRC_STATES_IFSTARTSTATE_H_ */
