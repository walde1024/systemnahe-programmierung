/*
 * SignThreeFirstState.h
 *
 *  Created on: Oct 14, 2015
 *      Author: walde
 */

#ifndef AUTOMAT_SRC_STATES_SIGNTHREESECONDSTATE_H_
#define AUTOMAT_SRC_STATES_SIGNTHREESECONDSTATE_H_

#include "State.h"

class SignThreeSecondState: public State {
public:
	SignThreeSecondState();
	virtual ~SignThreeSecondState();

	virtual bool isTransitionPossibleWith(char parameter);

	//True if transition could be executed.
	virtual bool performTransition(char parameter, Automat* automat);
};

#endif /* AUTOMAT_SRC_STATES_SIGNTHREESECONDSTATE_H_ */
