/*
 * CommentMiddleState.h
 *
 *  Created on: Oct 17, 2015
 *      Author: walde
 */

#ifndef AUTOMAT_SRC_STATES_COMMENTMIDDLESTATE_H_
#define AUTOMAT_SRC_STATES_COMMENTMIDDLESTATE_H_

#include "State.h"

class CommentMiddleState: public State {
private:

	char mNextTransitionParameter;

public:
	CommentMiddleState(char nextTransitionParameter);
	virtual ~CommentMiddleState();

	void setNextTransitionParameter(char parameter);

	//True if current state can perform a transition with the delivered parameter.
	virtual bool isTransitionPossibleWith(char parameter);

	//True if transition could be executed.
	virtual bool performTransition(char parameter, Automat* automat);
};

#endif /* AUTOMAT_SRC_STATES_COMMENTMIDDLESTATE_H_ */
