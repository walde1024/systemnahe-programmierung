/*
 * IdentifierStartState.h
 *
 *  Created on: Oct 15, 2015
 *      Author: walde
 */

#ifndef AUTOMAT_INCLUDES_STATES_IDENTIFIERFINALSTATE_H_
#define AUTOMAT_INCLUDES_STATES_IDENTIFIERFINALSTATE_H_

#include "State.h"

class IdentifierFinalState: public State {

	char *possibleTransitionSymbols = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

public:
	IdentifierFinalState();
	virtual ~IdentifierFinalState();

	//True if current state can perform a transition with the delivered parameter.
	bool isTransitionPossibleWith(char parameter);

	//True if transition could be executed.
	bool performTransition(char parameter, Automat* automat);
};

#endif /* AUTOMAT_INCLUDES_STATES_IDENTIFIERFINALSTATE_H_ */
