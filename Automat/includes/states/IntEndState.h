/*
 * IntEndState.h
 *
 *  Created on: Nov 1, 2015
 *      Author: walde
 */

#ifndef AUTOMAT_SRC_STATES_INTENDSTATE_H_
#define AUTOMAT_SRC_STATES_INTENDSTATE_H_

#include "State.h"

class IntEndState: public State {
public:
	IntEndState();
	virtual ~IntEndState();

	//True if current state can perform a transition with the delivered parameter.
	virtual bool isTransitionPossibleWith(char parameter);

	//True if transition could be executed.
	virtual bool performTransition(char parameter, Automat* automat);
};

#endif /* AUTOMAT_SRC_STATES_INTENDSTATE_H_ */
