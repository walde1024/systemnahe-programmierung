/*
 * NotAcceptedState.h
 *
 *  Created on: Oct 16, 2015
 *      Author: walde
 */

#ifndef AUTOMAT_INCLUDES_STATES_NOTACCEPTEDSTATE_H_
#define AUTOMAT_INCLUDES_STATES_NOTACCEPTEDSTATE_H_

#include "State.h"

class NotAcceptedState: public State {
public:
	NotAcceptedState();
	virtual ~NotAcceptedState();

	//True if current state can perform a transition with the delivered parameter.
	virtual bool isTransitionPossibleWith(char parameter);

	//True if transition could be executed.
	virtual bool performTransition(char parameter, Automat* automat);
};

#endif /* AUTOMAT_INCLUDES_STATES_NOTACCEPTEDSTATE_H_ */
