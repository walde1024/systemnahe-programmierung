/*
 * SignFinalState.h
 *
 *  Created on: Oct 14, 2015
 *      Author: walde
 */

#ifndef AUTOMAT_SRC_STATES_SIGNFINALSTATE_H_
#define AUTOMAT_SRC_STATES_SIGNFINALSTATE_H_

#include "State.h"

class Automat;

class SignFinalState: public State {
public:
	SignFinalState();
	virtual ~SignFinalState();

	//True if current state can perform a transition with the delivered parameter.
	virtual bool isTransitionPossibleWith(char parameter);

		//True if transition could be executed.
	virtual bool performTransition(char parameter, Automat* automat);
};

#endif /* AUTOMAT_SRC_STATES_SIGNFINALSTATE_H_ */
