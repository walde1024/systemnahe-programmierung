/*
 * SignThreeFirstState.h
 *
 *  Created on: Oct 14, 2015
 *      Author: walde
 */

#ifndef AUTOMAT_SRC_STATES_SIGNTWOFIRSTSTATE_H_
#define AUTOMAT_SRC_STATES_SIGNTWOFIRSTSTATE_H_

#include "State.h"

class SignTwoFirstState: public State {
public:
	SignTwoFirstState();
	virtual ~SignTwoFirstState();

	//True if current state can perform a transition with the delivered parameter.
	virtual bool isTransitionPossibleWith(char parameter);

	//True if transition could be executed.
	virtual bool performTransition(char parameter, Automat* automat);
};

#endif /* AUTOMAT_SRC_STATES_SIGNTWOFIRSTSTATE_H_ */
