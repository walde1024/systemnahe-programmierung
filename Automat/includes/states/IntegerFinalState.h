/*
 * IntegerStartState.h
 *
 *  Created on: Oct 15, 2015
 *      Author: walde
 */

#ifndef AUTOMAT_INCLUDES_STATES_INTEGERFINALSTATE_H_
#define AUTOMAT_INCLUDES_STATES_INTEGERFINALSTATE_H_

#include "State.h"

class IntegerFinalState: public State {
private:

	char *possibleTransitionSymbols = "0123456789";

public:
	IntegerFinalState();
	virtual ~IntegerFinalState();

	//True if current state can perform a transition with the delivered parameter.
	virtual bool isTransitionPossibleWith(char parameter);

	//True if transition could be executed.
	virtual bool performTransition(char parameter, Automat* automat);
};

#endif /* AUTOMAT_INCLUDES_STATES_INTEGERFINALSTATE_H_ */
