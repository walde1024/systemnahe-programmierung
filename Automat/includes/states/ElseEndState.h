/*
 * ElseEndState.h
 *
 *  Created on: Nov 1, 2015
 *      Author: walde
 */

#ifndef AUTOMAT_SRC_STATES_ELSEENDSTATE_H_
#define AUTOMAT_SRC_STATES_ELSEENDSTATE_H_

#include "State.h"

class ElseEndState: public State {
public:
	ElseEndState();
	virtual ~ElseEndState();

	//True if current state can perform a transition with the delivered parameter.
	virtual bool isTransitionPossibleWith(char parameter);

	//True if transition could be executed.
	virtual bool performTransition(char parameter, Automat* automat);

	void setNextTransitionParameter(char parameter);
};

#endif /* AUTOMAT_SRC_STATES_ELSEENDSTATE_H_ */
