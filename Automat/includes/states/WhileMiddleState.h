/*
 * WhileMiddleState.h
 *
 *  Created on: Oct 15, 2015
 *      Author: walde
 */

#ifndef AUTOMAT_INCLUDES_STATES_WHILEMIDDLESTATE_H_
#define AUTOMAT_INCLUDES_STATES_WHILEMIDDLESTATE_H_

#include "State.h"

class WhileMiddleState: public State {
private:

	char mNextTransitionParameter;

public:
	//nextTransitionParameter is the parameter which must be handed over to the performTransition method to perform a transition
	//Other parameters woun't be able to perform a transition to a non error state.
	WhileMiddleState(char nextTransitionParameter);
	virtual ~WhileMiddleState();

	void setNextTransitionParameter(char parameter);

	//True if current state can perform a transition with the delivered parameter.
	virtual bool isTransitionPossibleWith(char parameter);

	//True if transition could be executed.
	virtual bool performTransition(char parameter, Automat* automat);
};

#endif /* AUTOMAT_INCLUDES_STATES_WHILEMIDDLESTATE_H_ */
