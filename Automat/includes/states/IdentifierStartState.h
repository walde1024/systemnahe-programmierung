/*
 * IdentifierStartState.h
 *
 *  Created on: Oct 15, 2015
 *      Author: walde
 */

#ifndef AUTOMAT_INCLUDES_STATES_IDENTIFIERSTARTSTATE_H_
#define AUTOMAT_INCLUDES_STATES_IDENTIFIERSTARTSTATE_H_

#include "State.h"

class IdentifierStartState: public State {
private:

	char *possibleTransitionSymbols = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

public:
	IdentifierStartState();
	virtual ~IdentifierStartState();

	//True if current state can perform a transition with the delivered parameter.
	bool isTransitionPossibleWith(char parameter);

	//True if transition could be executed.
	bool performTransition(char parameter, Automat* automat);
};

#endif /* AUTOMAT_INCLUDES_STATES_IDENTIFIERSTARTSTATE_H_ */
