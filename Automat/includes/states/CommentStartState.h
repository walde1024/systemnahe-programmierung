/*
 * CommentStartState.h
 *
 *  Created on: Oct 17, 2015
 *      Author: walde
 */

#ifndef AUTOMAT_SRC_STATES_COMMENTSTARTSTATE_H_
#define AUTOMAT_SRC_STATES_COMMENTSTARTSTATE_H_

#include "State.h"

class CommentStartState: public State {
public:
	CommentStartState();
	virtual ~CommentStartState();

	//True if current state can perform a transition with the delivered parameter.
	virtual bool isTransitionPossibleWith(char parameter);

	//True if transition could be executed.
	virtual bool performTransition(char parameter, Automat* automat);
};

#endif /* AUTOMAT_SRC_STATES_COMMENTSTARTSTATE_H_ */
