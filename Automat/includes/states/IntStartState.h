/*
 * IntStartState.h
 *
 *  Created on: Nov 1, 2015
 *      Author: walde
 */

#ifndef AUTOMAT_SRC_STATES_INTSTARTSTATE_H_
#define AUTOMAT_SRC_STATES_INTSTARTSTATE_H_

#include "State.h"

class IntStartState: public State {
public:
	IntStartState();
	virtual ~IntStartState();

	//True if current state can perform a transition with the delivered parameter.
	virtual bool isTransitionPossibleWith(char parameter);

	//True if transition could be executed.
	virtual bool performTransition(char parameter, Automat* automat);
};

#endif /* AUTOMAT_SRC_STATES_INTSTARTSTATE_H_ */
