/*
 * SignAutomat.h
 *
 *  Created on: Oct 14, 2015
 *      Author: walde
 */

#ifndef AUTOMAT_SRC_IDENTIFIERAUTOMAT_H_
#define AUTOMAT_SRC_IDENTIFIERAUTOMAT_H_

#include "Automat.h"

class IdentifierAutomat: public Automat {
public:
	IdentifierAutomat();
	virtual ~IdentifierAutomat();
};

#endif /* AUTOMAT_SRC_IDENTIFIERAUTOMAT_H_ */
