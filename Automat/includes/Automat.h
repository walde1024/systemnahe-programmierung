/*
 * Automat.h
 *
 *  Created on: Jul 5, 2012
 *      Author: knad0001
 */

#ifndef Automat_H_
#define Automat_H_

#include "LangType.h"

class State;

class Automat {
private:
	const int MAX_STATES_COUNT = 16;

	State** mCurrentStates = new State*[MAX_STATES_COUNT];
	State** mNewStates = new State*[MAX_STATES_COUNT];

	int mCurrentStatesCount;
	int mNewStatesCount;

	State* mStartState;

	LangType mLangType;

	//Clears new states
	void clearNewStates();

	void addCurrentState(State* state);

	//Removes the current states
	void clearCurrentStates();

	//If there are new states, they replace the current states
	void copyNewStatesToCurrentStates();

public:
	Automat();
	virtual ~Automat();

	//Adds a state
	void addNewState(State* state);
	//Returns true if a transition was performed
	bool performTransition(char parameter);

	void setStartState(State* state);

	bool isTransitionPossible(char parameter);

	bool isInFinalState();

	void resetToStartState();

	LangType getLangType();

protected:
	void setLangType(LangType type);
};

#endif /* Automat_H_ */
