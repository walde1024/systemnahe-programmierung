/*
 * ElseAutomat.h
 *
 *  Created on: Nov 1, 2015
 *      Author: walde
 */

#ifndef AUTOMAT_SRC_ELSEAUTOMAT_H_
#define AUTOMAT_SRC_ELSEAUTOMAT_H_

#include "Automat.h"

class ElseAutomat: public Automat {
public:
	ElseAutomat();
	virtual ~ElseAutomat();
};

#endif /* AUTOMAT_SRC_ELSEAUTOMAT_H_ */
