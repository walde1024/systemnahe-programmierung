/*
 * SignAutomat.h
 *
 *  Created on: Oct 14, 2015
 *      Author: walde
 */

#ifndef AUTOMAT_SRC_INTEGERAUTOMAT_H_
#define AUTOMAT_SRC_INTEGERAUTOMAT_H_

#include "Automat.h"

class IntegerAutomat: public Automat {
public:
	IntegerAutomat();
	virtual ~IntegerAutomat();
};

#endif /* AUTOMAT_SRC_INTEGERAUTOMAT_H_ */
