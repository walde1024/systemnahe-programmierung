/*
 * ReadAutomat.h
 *
 *  Created on: Nov 1, 2015
 *      Author: walde
 */

#ifndef AUTOMAT_INCLUDES_READAUTOMAT_H_
#define AUTOMAT_INCLUDES_READAUTOMAT_H_

#include "../includes/Automat.h"

class ReadAutomat: public Automat {
public:
	ReadAutomat();
	virtual ~ReadAutomat();
};

#endif /* AUTOMAT_INCLUDES_READAUTOMAT_H_ */
