/*
 * AutomatWrapper.h
 *
 *  Created on: Oct 14, 2015
 *      Author: walde
 */

#ifndef AUTOMAT_AUTOMATWRAPPER_H_
#define AUTOMAT_AUTOMATWRAPPER_H_

#include "../includes/LangType.h"

class Automat;

class AutomatWrapper {
private:
	const int MACHINES_COUNT = 10;
	Automat** mMachines = new Automat*[MACHINES_COUNT];

	int mTransitionCount;

public:
	AutomatWrapper();
	virtual ~AutomatWrapper();

	//True if one of the wrapped state machines can perform a state transition
	bool isTransitionPossible(char parameter);
	//True if transition was successfully
	bool performTransition(char parameter);
	//Starts again from the start states
	void resetToStartStates();
	//True if the input is accepted by the automat
	bool isAccepting();
	//Returns the LangType which is accepted by this automat
	LangType getAcceptedLangType();

	int getTransitionCount();
};

#endif /* AUTOMAT_AUTOMATWRAPPER_H_ */
