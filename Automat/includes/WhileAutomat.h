/*
 * SignAutomat.h
 *
 *  Created on: Oct 14, 2015
 *      Author: walde
 */

#ifndef AUTOMAT_SRC_WHILEAUTOMAT_H_
#define AUTOMAT_SRC_WHILEAUTOMAT_H_

#include "Automat.h"

class WhileAutomat: public Automat {
public:
	WhileAutomat();
	virtual ~WhileAutomat();
};

#endif /* AUTOMAT_SRC_WHILEAUTOMAT_H_ */
