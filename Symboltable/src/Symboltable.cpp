/*
 * Symboltable.cpp
 *
 *  Created on: Sep 26, 2012
 *      Author: knad0001
 */

#include "../includes/Symboltable.h"
#include "../includes/SymbolKey.h"
#include "../includes/Bucket.h"
#include "../includes/BucketEntry.h"

#include <iostream>

Symboltable::Symboltable() {
	mSymbolCount = 0;
	mRows = 0;

	mSymbolBuffer = new char*[1];
	mSymbolBuffer[0] = new char[BUFFER_SIZE];
}

SymbolKey* Symboltable::addLexem(char *identifier, int size) {
	return insertIntoBucket(identifier, size);
}

SymbolKey* Symboltable::insertIntoStringTable(char* identifier, int size) {
	//Check if new row musst be appended to the string table
	if ((mSymbolCount + size + 1) > BUFFER_SIZE -1) {
		addRowToBuffer(size);
	}

	//Copy lexem to string table
	int startIndex = mSymbolCount;
	for (int i = 0; i < size; i++) {
		*(mSymbolBuffer[mRows]+mSymbolCount) = *(identifier+i);
		mSymbolCount++;
	}
	*(mSymbolBuffer[mRows]+mSymbolCount) = '\0';
	mSymbolCount++;

	return new SymbolKey(mRows, startIndex);
}

char* Symboltable::getLexem(SymbolKey *key) {
	return mSymbolBuffer[key->getRow()]+key->getIndex();
}

char* Symboltable::getValue(SymbolKey *key) {
	return mSymbolBuffer[key->getRow()]+key->getIndex();
}

void Symboltable::addRowToBuffer(int size) {
	mRows++;
	char **t = 0;

	t = new char*[mRows];
	for (int i = 0; i < mRows; i++) {
		t[i] = mSymbolBuffer[i];
	}
	if ((size + 1) > BUFFER_SIZE) {
		t[mRows] = new char[BUFFER_SIZE = size + BUFFER_SIZE]; //Increase Buffer Size
	}
	else {
		t[mRows] = new char[BUFFER_SIZE];
	}

	mSymbolBuffer = t;
	mSymbolCount = 0;
}

SymbolKey* Symboltable::insertIntoBucket(char* identifier, int size) {
	int h = hash(identifier);

	BucketEntry *bucketEntry = mBuckets[h].findBucketEntryOfLexem(identifier, size, this);
	if (!bucketEntry) {
		SymbolKey *key = insertIntoStringTable(identifier, size);
		mBuckets[h].insertIntoBucket(getLexem(key), key);

		return key;
	}
	else {
		return bucketEntry->getSymbolKey();
	}
}

SymbolKey* Symboltable::addKeyValue(char* key, char* value, int keySize, int valueSize) {
	int h = hash(key);

	BucketEntry *bucketEntry = mBuckets[h].findBucketEntryByKey(key, keySize, this);
	SymbolKey *skey = insertIntoStringTable(value, valueSize);

	if (!bucketEntry) {
		mBuckets[h].insertKeyValueIntoBucket(key, getValue(skey), skey, keySize);
		return skey;
	}
	else {
		bucketEntry->update(getValue(skey), skey);
		return bucketEntry->getSymbolKey();
	}
}

char* Symboltable::getKeyValue(char* key, int keySize) {
	int h = hash(key);
	BucketEntry *bucketEntry = mBuckets[h].findBucketEntryByKey(key, keySize, this);
	if (!bucketEntry) {
		return 0;
	}
	else {
		return getValue(bucketEntry->getSymbolKey());
	}
}

int Symboltable::hash(const char *str) {
    unsigned int h = 0;
    while (*str)
       h = h << 1 ^ *str++;
    return h % BUCKET_SIZE;
}

Symboltable::~Symboltable() {
	for (int i = 0; i < mRows; i++) {
		delete mSymbolBuffer[i];
	}
	delete mSymbolBuffer;
}
