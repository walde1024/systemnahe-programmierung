/*
 * Bucket.cpp
 *
 *  Created on: Oct 18, 2015
 *      Author: walde
 */

#include "../includes/Bucket.h"
#include "../includes/SymbolKey.h"
#include "../includes/Symboltable.h"
#include "../includes/BucketEntry.h"
#include <string.h>

Bucket::Bucket() {

}

Bucket::~Bucket() {
	delete mFirst;
	delete mLast;
}

BucketEntry* Bucket::insertIntoBucket(char *lexem, SymbolKey* key) {
	if (!mFirst) {
		mFirst = new BucketEntry(lexem, key);
		mLast = mFirst;
	}
	else {
		BucketEntry* e = new BucketEntry(lexem, key);
		mLast->setNextEntry(e);
		mLast = e;
	}

	return mLast;
}

BucketEntry* Bucket::insertKeyValueIntoBucket(char* key, char* value, SymbolKey* skey, int keySize) {
	BucketEntry* e;

	if (!mFirst) {
		e = mFirst = new BucketEntry(value, skey);
		mLast = mFirst;
	}
	else {
		e = new BucketEntry(value, skey);
		mLast->setNextEntry(e);
		mLast = e;
	}

	char* bucketKey = 0;
	//copy key
	bucketKey = new char[keySize+1];
	for (int i = 0; i < keySize; i++) {
		*(bucketKey+i) = *(key+i);
	}
	*(bucketKey+keySize) = '\0';

	mLast->setKey(bucketKey);

	return mLast;
}

BucketEntry* Bucket::findBucketEntryOfLexem(char *lexem, int size, Symboltable* table) {
	BucketEntry* e = mFirst;
	SymbolKey* k;
	bool equal = false;

	while (true) {
		equal = false;
		if (!e) {
			return 0;
		}

		k = e->getSymbolKey();
		char* tableString = table->getLexem(k);

		/*for (int i = 0; i < size; i++) {
			if (*(tableString+i) != *(lexem+i)) {
				equal = false;
				break;
			}
			else if (*(tableString+i) == '\0') {
				equal = false;
				break;
			}
			else {
				equal = true;
			}
		}*/
		equal = (strncmp(tableString, lexem, size) == 0);

		if (equal && tableString[size] == '\0')
			return e;

		e = e->getNextBucketEntry();
	}

	return 0;
}

BucketEntry* Bucket::findBucketEntryByKey(char* key, int size, Symboltable* table) {
	BucketEntry* e = mFirst;
	SymbolKey* k;
	bool equal = false;

	while (true) {
		equal = false;
		if (!e) {
			return 0;
		}

		char* bucketKey = e->getKey();

		for (int i = 0; i < size; i++) {
			if (*(bucketKey+i) != *(key+i)) {
				equal = false;
				break;
			}
			else if (*(bucketKey+i) == '\0') {
				equal = false;
				break;
			}
			else {
				equal = true;
			}
		}

		if (equal)
			return e;

		e = e->getNextBucketEntry();
	}

	return 0;
}

