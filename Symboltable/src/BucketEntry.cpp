/*
 * BucketEntry.cpp
 *
 *  Created on: Oct 18, 2015
 *      Author: walde
 */

#include "../includes/BucketEntry.h"
#include "../includes/SymbolKey.h"

BucketEntry::BucketEntry(char *lexem, SymbolKey* key) {
	mLexem = lexem;
	mSymbolKey = key;
	mNextEntry = 0;
	mKey = new char[1];
	*mKey = '\0';
}

BucketEntry::~BucketEntry() {
	delete mNextEntry;
}

void BucketEntry::setNextEntry(BucketEntry* entry) {
	mNextEntry = entry;
}

SymbolKey* BucketEntry::getSymbolKey() {
	return mSymbolKey;
}

BucketEntry* BucketEntry::getNextBucketEntry() {
	return mNextEntry;
}

char* BucketEntry::getKey() {
	return mKey;
}

void BucketEntry::setKey(char* key) {
	mKey = key;
}

//Sets new lexem and key pointers
void BucketEntry::update(char *lexem, SymbolKey* key) {
	mLexem = lexem;
	mSymbolKey = key;
}
















