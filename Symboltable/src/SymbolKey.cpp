/*
 * SymbolKey.cpp
 *
 *  Created on: Oct 18, 2015
 *      Author: walde
 */

#include "../includes/SymbolKey.h"

SymbolKey::SymbolKey(int row, int index) {
	mRow = row;
	mIndex = index;
}

SymbolKey::~SymbolKey() {
	// TODO Auto-generated destructor stub
}

int SymbolKey::getIndex() {
	return mIndex;
}

int SymbolKey::getRow() {
	return mRow;
}
