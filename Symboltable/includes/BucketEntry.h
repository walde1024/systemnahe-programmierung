/*
 * BucketEntry.h
 *
 *  Created on: Oct 18, 2015
 *      Author: walde
 */

#ifndef SYMBOLTABLE_SRC_BUCKETENTRY_H_
#define SYMBOLTABLE_SRC_BUCKETENTRY_H_

class SymbolKey;

class BucketEntry {
private:

	BucketEntry* mNextEntry;

	SymbolKey* mSymbolKey;

	//Pointer to lexem in symbol table array.
	char *mLexem;

	//For key-value entries
	char *mKey;

public:
	BucketEntry(char *lexem, SymbolKey* key);
	virtual ~BucketEntry();

	void setNextEntry(BucketEntry* entry);
	BucketEntry* getNextBucketEntry();

	SymbolKey* getSymbolKey();

	char* getKey();
	void setKey(char* key);

	//Sets new lexem/value and key pointers
	void update(char *lexem, SymbolKey* key);
};

#endif /* SYMBOLTABLE_SRC_BUCKETENTRY_H_ */
