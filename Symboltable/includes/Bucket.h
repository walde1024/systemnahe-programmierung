/*
 * Bucket.h
 *
 *  Created on: Oct 18, 2015
 *      Author: walde
 */

#ifndef SYMBOLTABLE_SRC_BUCKET_H_
#define SYMBOLTABLE_SRC_BUCKET_H_

class BucketEntry;
class SymbolKey;
class Symboltable;

class Bucket {
private:

	BucketEntry *mFirst;
	BucketEntry *mLast;

public:
	Bucket();
	virtual ~Bucket();

	//True if entry was created and false if it was already available
	BucketEntry* insertIntoBucket(char *lexem, SymbolKey* key);
	BucketEntry* insertKeyValueIntoBucket(char* key, char* value, SymbolKey* skey, int keySize);

	//The BucketEntry or null
	BucketEntry* findBucketEntryOfLexem(char *lexem, int size, Symboltable* table);
	BucketEntry* findBucketEntryByKey(char* key, int size, Symboltable* table);
};

#endif /* SYMBOLTABLE_SRC_BUCKET_H_ */
